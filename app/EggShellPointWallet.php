<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EggShellPointWallet extends Model
{
    //

    protected $fillable = ['balance','user_id'];
}
