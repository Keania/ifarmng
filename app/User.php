<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','address','suscription_type','role','token','email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin(){
        return $this->role == 7 ? true : false; 
    }

    public function isDropOff(){
      
        return $this->suscription_type == "drop off" ? true : false;
    }

    public function isDoorToDoor(){
        return $this->suscription_type == "door to door" ? true : false;
    }

   

    public function isSuscriber(){

        return $this->role == 2 ? true : false; 
    }

    

    public function isUser(){

        return $this->role == 1 ? true : false; 
    }

   public function ES_Points(){
       return $this->hasMany('App\EggShellPoint');
   }

   public function ESP_Wallet(){
       return $this->hasOne('App\EggShellPointWallet');
   }

   public function suscription_orders(){
       return $this->hasMany('App\SuscriberOrder');
   }

   public function cashRequestOrders(){
       return $this->hasMany('App\SuscriberCashOutOrder');
   }

   public function scopeSuscribers($query)
   {
       return $query->where('role',2);
   }

   public function scopeSystemUsers($query)
   {
       return $query->where('role',1);
   }

   public function orders(){
       return $this->hasMany('App\Order');
   }
   
   public function email_verified()
   {
     return isset($this->email_verified_at) && is_null($this->token);

   }




}
