<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Volunteer;
use App\EggShellPoint;
use App\EggShellPointWallet as Wallet;
use Illuminate\Support\Facades\DB;
use App\Order;
use Illuminate\Database\QueryException;

class AdminDashboardController extends Controller
{
    //

    public function __construct()
    {
        
        $this->middleware('admin');
    }


    public function index(){

        return view('admin.dashboard');
    }

    public function users(){

        $users = User::systemUsers()->get();
        return view('admin.all_users',['users'=>$users]);
    }

    public function suscribers(){

        $suscribers = User::suscribers()->get();
        return view('admin.suscribers',['suscribers'=>$suscribers]);
    }

    public function volunteers(){
        $volunteers  = Volunteer::all();
        return view('admin.volunteers',['volunteers'=>$volunteers]);
    }

    public function addShellPoints(Request $request){

        $suscriber = User::find($request->id);
        return view('admin.pointsCard',['suscriber'=>$suscriber]);
    }

    public function addShellPointsPost(Request $request){
        try{

            DB::beginTransaction();
            $point = $this->saveShellPoint($request);
           
            $this->updatePointWallet($point);

        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        DB::commit();

        return redirect()->back()->with(['success'=>'Points were added Successfully']);
    }

    public function saveShellPoint($request){

        $point = new EggShellPoint();
        $point->weight = $request->weight;
        $point->points = $request->points;
        $point->description = $request->description;
        $point->user_id = $request->user_id;
        $point->save();
        return $point;
    }

    public function updatePointWallet($point){
    
        $wallet = Wallet::firstOrNew(['user_id'=>$point->user_id]);
        $wallet->balance += $point->points;
        $wallet->save();
        
    }

    public function ProductOrders()
    {
        $orders =  Order::fulfilled()->get();
        return view('admin.orders.product');
    }

   

    public function markOrderAsFulfilled(Request $request,$id)
    {
        try{
            $order = Order::find($id);
            $order->update(['status'=>2]);
        }catch(QueryException $e){

            return redirect()->back()->with(['error'=>$e->errorInfo[2]]);
        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

    }


    public function previewOrder(Request $request, $id)
    {
        $order = Order::find($id);
        return view('admin.orders.preview',['order'=>$order]);
    }

    public function cashRequestOrders(){
        return view('admin.orders.cashRequest');
    }

    public function suscriptionProductOrders()
    {
        return view('admin.orders.suscription');
    }
    
}
