<?php

namespace App\Http\Controllers\Volunteer;

use App\Volunteer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phone'=>['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:volunteers'],
            'current_location'=>['required'],
            'service_type'=>['required','string'],
            'duration_of_service'=>['required','string'],
            'availability'=>['required','string'],
            'hear_about_us'=>['required','string']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
       
       
        return Volunteer::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'current_location'=>$data['current_location'],
            'phone'=>$data['phone'],
            'service_type'=>$data['service_type'],
            'duration_of_service'=>$data['duration_of_service'],
            'availability'=>$data['availability'],
            'hear_about_us'=>$data['hear_about_us'],
        ]);
    }

   public function showRegistrationForm()
   {
       return view('auth.volunteer.register');
   }

   /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
       
        $this->validator($request->all())->validate();
       

        $user = $this->create($request->all());

        session()->flash('success','Registration was successful');

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }


    // after registration send the user a registration email
    public function registered($request,$user)
    {
        try{

            $this->sendRegistrationMail($user);

        }catch(\Swift_TransportException $e){

            return redirect()->route('volunteer.register.form')->with(['error'=>$e->getMessage()]);

        }catch(Exception $e){

            return redirect()->route('volunteer.register.form')->with(['error'=>$e->getMessage()]);
        }

        return redirect()->route('volunteer.register.form');

        
    }

   public function sendRegistrationMail($user)
   {
      
   }
}
