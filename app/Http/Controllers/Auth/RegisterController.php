<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\EmailVerification;
use Swift_TransportException;
use Exception;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone'=> ['required','numeric']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
            $token = implode(explode(' ',$data['name'])).str_random(10);
            
           
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'address'=>$data['address'],
                'phone'=>$data['phone'],
                'suscription_type'=> isset($data['suscription_type']) ? $data['suscription_type'] : null,
                'role'=>isset($data['role']) ? $data['role'] : 1,
                'password' => Hash::make($data['password']),
                'token'=>$token,
            ]);
       
       
    }

    public function showSuscriberRegisterForm(){
        return view('auth.suscriber_register');
    }

    public function registered(Request $request, $user)
    {
        try {

           Mail::send('verify_email',['name'=>$user->name,'token'=>$user->token],function($message) use ($user) {
               
               $message->to($user->email);
               $message->from('support@ifarmng.com','Ifarm NG');
               $message->subject('Account Verification Email');
               
               
               
           });

        }catch(Swift_TransportException $e){
           

            session()->reflash('error',$e->getMessage());
        }
        catch(Exception $e){
           

            session()->reflash('error',$e->getMessage());
        }
    }


    
}
