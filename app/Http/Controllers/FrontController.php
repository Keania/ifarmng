<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog\Post as Post;
use App\Product;
use App\SuscribeProduct;
use Illuminate\Database\QueryException;
use App\SuscriberOrder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\User;
use Illuminate\Support\Facades\Hash;
use Exception;
use Swift_TransportException;

class FrontController extends Controller
{
    //

    public function index(){

        return view('front.home');
    }
    
    public function gallery()
    {
        return view('front.gallery');
    }

    public function offers(){
       
        return view('front.offers');
    }

    public function reviewOffer(Request $request){

        $offer = SuscribeProduct::find($request->id);
        
        return view('front.confirm_offer',['offer'=>$offer]);
    }



    public function blog_index(){

        $posts = Post::latest()->paginate(6);
        
        return view('front.blog.index',['posts'=>$posts]);
    }


    public function post(Request $request){

        $post = Post::find($request->id);
        $latest = Post::latest()->take(5)->get();
       

        return view('front.blog.single',['post'=>$post,'latest'=>$latest]);
    }

    public function shop_index(){


        $products = Product::latest()->paginate(15);
        return view('front.shop.index',['products'=>$products]);
    }

    public function shop_single(Request $request){

        $product = Product::find($request->product_id);
        return view('front.shop.single',['product'=>$product]);
    }

    public function about(){
        return view('front.about');
    }

    public function contact(){

        return view('front.contact');
    }
    
    public function sendContactMail(Request $request)
    {
        try{
           
            $sender_message = $request->sender_message;
            
            Mail::send('contact_email',['sender_message'=>$sender_message],function($message) use ($request) {
                
                $message->to('support@ifarmng.com');
                $message->from($request->sender_email,$request->sender_name);
                $message->subject('Contact Message From Ifarm Website');
            });
        }catch(Swift_TransportException $e){
           

           return redirect()->back()->with(['error'=>$e->getMessage()]);
        }catch(Exception $e){
            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
        
        return redirect()->back()->with(['success'=>'Your mail has been sent']);
    }


    public function verifyEmail(Request $request)
    {
        
        $token = $request->token;
        
        try {

            $user  = User::where('token',$token)->first();
            
            if( !isset($user)){

                throw new Exception('This verification token is expired');
            }
            $user->update(['token'=>null,'email_verified_at'=>now()]);
         

        }catch(QueryException $e){

            return redirect()->route('home')->with(['error'=>$e->getMessage()]);

        }catch(Exception $e){

            return redirect()->route('home')->with(['error'=>$e->getMessage()]);
        }

        return redirect()->route('dashboard')->with(['success'=>' Your email has been verified ']);

    }
}
