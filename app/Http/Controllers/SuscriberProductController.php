<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\SuscribeProduct;
use Mockery\Exception;
use Illuminate\Database\QueryException;

class SuscriberProductController extends Controller
{
    //

    public function __construct()
    {
        $this->storage = Storage::disk('suscribe_products');
    }

    public function index(){
        $suscribe_products = SuscribeProduct::latest()->get();

        return view('admin.suscribe_products.index',['suscriber_products'=>$suscribe_products]);
    }
    public function addOfferForm(){
        return view('admin.suscribe_products.add');
    }

    public function addProduct(Request $request){
       

        try{

            $this->saveProductModel($request);

        }catch(QueryException $e){

            return redirect()->back()->with(['error'=>$e->errorInfo[2]]);
        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);

        }

        return redirect()->back()->with(['success'=>'Product was successfully added']);

    }

    public function saveProductModel($request){

        $suscribe_product = new SuscribeProduct();
        $suscribe_product->name = $request->name;
        $filename = null;
        if($request->hasFile('image') && $request->file('image')->isValid()){

            $filename = $this->storage->putFile('',$request->file('image'));
        }
        $suscribe_product->egg_shell_points = $request->egg_shell_points;
        
        if($filename){
            $suscribe_product->image = $filename;
        }
       

        $suscribe_product->save();

    }

    public function delete(Request $request){

       try{ 
            $suscribe_product = SuscribeProduct::find($request->id);

            if($suscribe_product->image){
                $this->storage->delete($suscribe_product->image);
            }

            $suscribe_product->delete();

        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->back()->with(['success'=>'Product Successfully Removed']);

    }

    public function updateProduct(Request $request){

        try {

            $this->updateProductModel($request);
        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->back()->with(['success'=>'Product Successfully Added']);
    }


    public function updateProductModel(Request $request){

        $suscribe_product = SuscribeProduct::find($request->product_id);

        !isset($request->name) ? : $suscribe_product->name = $request->name;
        !isset($request->egg_shell_points) ? : $suscribe_product->egg_shell_points = $request->egg_shell_points;

        if($request->hasFile('image') && $request->file('image')->isValid()){

            $filename = $this->storage->putFile('',$request->file('image'));
            $suscribe_product->image = $filename;

        }

        $suscribe_product->save();
    }


}
