<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Product;


class ProductController extends Controller
{
    //

    public function __construct(){

        $this->storage = Storage::disk('products');
    }


    public function add(){

        return view('admin.products.add');
    }

    public function index(){
        
        $products = Product::latest()->get();
        return view('admin.products.index',['products'=>$products]);
    }

    public function updateProduct(Request $request){

        try{

            $this->saveProductUpdateModel($request);

        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->back()->with(['success'=>'Changes were Successful']);
    }

    

    public function delete(Request $request){

        try{

            $product = Product::find($request->product_id);
            $this->storage->delete($product->photo);
            $product->delete();

        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->back()->with(['success'=>'Product Successfully removed']);

    }


    public function addProduct(Request $request){

        try{

            $this->saveProductModel($request);

        }catch(Expection $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->back()->with(['success'=>'Product Uploaded']);
    }

    public function saveProductModel($request){

        $product = new Product();
        $product->name = $request->product_name;
        $product->price = $request->product_price;
        $product->desc = $request->product_desc;
      

        if($request->hasFile('product_photo') && $request->file('product_photo')->isValid()){

            $filename = $this->storage->putFile('',$request->file('product_photo'));
        }

        $product->photo  = $filename;

        $product->save();
    }


    public function saveProductUpdateModel($request){

       
        $product = Product::find($request->product_id);
        !isset($request->product_name) ? : $product->name = $request->product_name;
        ! isset($request->product_price) ? : $product->price  = $request->product_price;
      
        !isset($request->product_desc) ? : $product->desc = $request->product_desc;

        if($request->hasFile('product_photo') && $request->file('product_photo')->isValid()){

            $this->storage->delete($product->photo);
            $filename = $this->storage->putFile('',$request->file('product_photo'));
            $product->photo = $filename;

        }

        $product->save();
    }



}
