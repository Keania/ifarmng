<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Blog\Post;

class BlogController extends Controller
{
    //

    public function __construct(){

        $this->storage = Storage::disk('posts');
    }

    public function index(){
        $posts = Post::latest()->get();

        return view('admin.blog.index',['posts'=>$posts]);
    }


    public function add(){

        return view('admin.blog.upload');
    }

    public function addPost(Request $request){

        try{

            $this->savePostModel($request);

        }catch(\Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->back()->with(['success'=>'Post Updated Successfully']);
    }

    public function savePostModel($request){

        $post = new Post();
        $post->post_title = $request->post_title;
        $post->post_content = $request->post_content;
        if($request->hasFile('post_photo') && $request->file('post_photo')->isValid()){
            $filename = $this->storage->putFile('',$request->file('post_photo'));
            $post->image_file = $filename;
        }

        $post->save();
    }


    public function delete(Request $request){

       try{

            $post = Post::find($request->post_id);
            $this->storage->delete($post->image_file);
            $post->delete();

       }catch(Exception $e){

        return redirect()->back()->with(['error'=>$e->getMessage()]);
    }

        return redirect()->back()->with(['success'=>'Post Successfully removed']);

    }


    public function updatePost(Request $request){

        try{

            $this->updatePostModel($request);

        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
    
            return redirect()->back()->with(['success'=>'Post Successfully updated']);
    }

    public function updatePostModel($request){

        $post = Post::find($request->post_id);
        !isset($request->post_title) ? : $post->post_title = $request->post_title;
        !isset($request->post_content)?: $post->post_content = $request->post_content;

        if($request->hasFile('post_photo') && $request->file('post_photo')->isValid()){

            $this->storage->delete($post->image_file);
            $filename = $this->storage->putFile('',$request->file('post_photo'));
            $post->image_file = $filename;
        }

        $post->save();
    }
}

