<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Product;

class CartController extends Controller
{
    //

    public function add(Request $request){
       
        $product = Product::find($request->id);
        $image = $product->photo;
       
        Cart::add(['id'=>$product->id,'name'=>$product->name,'qty'=>1,'price'=>$product->price,'options'=>['image'=>$image,'desc'=>$product->desc]]);
        return([Cart::count(),$product]);
    }


    public function remove(Request $request){
       
        try{
            Cart::remove($request->id);
        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

            return redirect()->back()->with(['success'=>'Cart Item Successfully Removed']);

        
    }


    public function index(){
        $cartItems = Cart::content();
        return view('front.cart',['cartItems'=>$cartItems]);
    }

    public function update(){

    }

    public function destroy(){

        try{

            Cart::destroy();

        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

            return redirect()->back()->with(['success'=>'Cart destroyed']);

    }


}
