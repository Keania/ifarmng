<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Address;
use Swift_TransportException;

class UserDashboardController extends Controller
{
    //

    public function __construct()
    {   
        $this->middleware('auth');
        
    }

    public function index(){
       
        return view('user.dashboard');
    }

    public function updateProfile(Request $request){

        try {

            $this->updateProfileInfo($request);
        }catch(Exception $e){
            dd('An error occured');
            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->back()->with(['success'=>'Profile Updated Successfully']);
        
    }


    public function updateProfileInfo($request){
        $user  = Auth::user();
        !isset($request->name) ? : $user->name = $request->name;
        !isset($request->phone_number) ? : $user->phone_number = $request->phone_number;
        !isset($request->address) ? : $user->address = $request->address;
        $user->save();
    }

    public function updateAddressModel($request){

        $user = Auth::user();
        $address = Address::firstOrNew(['user_id'=>$user->id]);

        ! isset($request->zipCode)?: $address->zipCode = $request->zipCode;
        ! isset($request->phone)?: $address->phone = $request->phone;
        ! isset($request->home_address)?: $address->home_address = $request->home_address;
        ! isset($request->city) ? : $address->city = $request->city;
        ! isset($request->state) ?: $address->state = $request->state;

        $address->save();

    }
    
    public function resendLink()
    {
        try{
            $user = Auth::user();
            
             Mail::send('verify_email',['name'=>$user->name,'token'=>$user->token],function($message) use ($user) {
               
               $message->to($user->email);
               $message->from('support@ifarmng.com','Ifarm NG');
               $message->subject('Account Verification Email');
               
           });
            
        }catch(Swift_TransportException $e){
           

           return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
        catch(Exception $e){
           

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
        
        return redirect()->back()->with(['success'=>'Email has been successively sent. ']);
    }

    
}
