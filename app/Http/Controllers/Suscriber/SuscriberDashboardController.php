<?php

namespace App\Http\Controllers\Suscriber;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EggShellPoint;
use App\Product;
use Illuminate\Support\Facades\Auth;
use App\SuscriberOrder;
use App\SuscribeProduct;
use App\SuscriberCashOutOrder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class SuscriberDashboardController extends Controller
{
    //


    public function __construct()
    {
        //$this->middleware('volunteer');
    }

    public function index(){

        

        return view('suscriber.dashboard');
    }

 


    public function saveOfferRequest(Request $request){
        $user = Auth::user();
        $product = SuscribeProduct::find($request->offer_id);

        if($user->ESP_Wallet->balance < $product->egg_shell_points){

            return redirect()->back()->with(['error'=>'Your wallet balance cannot afford this product']);
        }
        
        try{
            DB::beginTransaction();
            
            $points = $product->egg_shell_points;
            $old_balance = $user->ESP_Wallet->balance;
            $user->ESP_Wallet->update(['balance'=>$old_balance - $points]);
            $suscribe_order = new SuscriberOrder();
            $suscribe_order->user_id = $user->id;
            $suscribe_order->order_reference = '#'.$user->id.time().str_random(3);
            $suscribe_order->offer_ordered = $request->offer_id;
            $suscribe_order->save();

        }catch(QueryException $e){
            DB::rollBack();

            return redirect()->back()->with(['error'=>$e->errorInfo[2]]);
        }catch(Exception $e){
            DB::rollBack();

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
        DB::commit();

        return redirect()->back()->with(['success'=>'Offer was successfully sent']);
    }

    public function saveCashoutRequest(Request $request)
    {
        try{
            DB::beginTransaction();

            $this->cashOutOrder($request);

        }catch(QueryException $e){
            DB::rollBack();

            return redirect()->back()->with(['error'=>$e->errorInfo[2]]);

        }catch(Exception $e){
            DB::rollBack();


            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        DB::commit();

        return redirect()->back()->with(['success'=>'Order was successfully sent']);
    }


    public function cashOutOrder(Request $request)
    {
      
        $user  = Auth::user();
        $points = $user->ESP_Wallet->balance;
        $user->ESP_Wallet->update(['balance'=>0]);
        $suscribe_order = new SuscriberCashOutOrder;
        $suscribe_order->account_name = $request->account_name;
        $suscribe_order->account_number = $request->account_number;
        $suscribe_order->bank = $request->bank;
        $suscribe_order->egg_shell_point = $points;
        $suscribe_order->user_id  = $user->id;
        $suscribe_order->amount  = getMoneyEquivalent($points);
        $suscribe_order->save();

    }


    
}
