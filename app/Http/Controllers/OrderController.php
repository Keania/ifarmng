<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Order;
use App\OrderItem;
use Paystack;
use Cookie;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use League\Flysystem\Exception;
use Illuminate\Support\Facades\Auth;
use App\SuscriberCashOutOrder;
use App\SuscriberOrder;
use Illuminate\Database\QueryException;

class OrderController extends Controller
{
    //


    public function review(Request $request){

           if($request->cookie('order')){

               return view('front.review',['order'=>json_decode($request->cookie('order'))]);
           }
           return redirect()->route('cart.index')->with(['error'=>'No order has been created ']);
           
    }

    public function createOrder(){
    
        try {

            if(is_null(Auth::user())){
                throw new Exception('You are not logged in . Login to continue');
            }

            $order = new Order;
            $order->ref = Paystack::genTranxRef();
            $order->status = 1;
            $order->amount = Cart::total();
            $order->user_id = Auth::user()->id;
            $order->save();

        }
        catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->route('order.review')->withCookie(cookie()->make('order',$order),144);

    }

    public function cancel(Request $request,$id){

        try{

            $order = Order::find($id);
            $order->delete();

        }catch(FatalThrowableError $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->route('cart.index')->with(['success'=>'Order has been cancelled'])->withCookie(cookie()->forget('order'));
    }

    public function error()
    {

        return view('order_error');
    }

    public function success(Request $request)
    {
        if(! isset($request->order)){
            return redirect()->back()->with(['error','No order read. No access']);
        }
        $order = Order::find($request->order);
        return view('order_success',['order'=>$order]);
    }


    public function markAsSuccessCashRequest(Request $request)
    {
        $order = SuscriberCashOutOrder::find($request->id);
        $this->updateStatus($order,2);
        return redirect()->back()->with(['success'=>'Status updated']);
       
    }

    public function markAsDeclineCashRequest(Request $request)
    {
        $order = SuscriberCashOutOrder::find($request->id);
        $this->updateStatus($order,3);
        return redirect()->back()->with(['success'=>'Status updated']);
    }

    public function markAsSuccessSuscriberOrder(Request $request)
    {
        $order = SuscriberOrder::find($request->id);
        $this->updateStatus($order,2);
        return redirect()->back()->with(['success'=>'Status updated']);
    }

    public function markAsDeclineSuscriberOrder(Request $request)
    {
        $order = SuscriberOrder::find($request->id);
        $this->updateStatus($order,3);
        return redirect()->back()->with(['success'=>'Status updated']);
    }

    public function markAsSuccessOrder(Request $request)
    {
        $order = Order::find($request->id);
        $this->updateStatus($order,2);
        return redirect()->back()->with(['success'=>'Status updated']);
    }

    public function markAsDeclineOrder(Request $request)
    {
        $order = Order::find($request->id);
        $this->updateStatus($order,3);
        return redirect()->back()->with(['success'=>'Status updated']);
    }
     

    public function updateStatus($model,$status){

        try {

           
            $model->update(['status'=>$status]);

        }catch(QueryException $e){
            return redirect()->back()->with(['error'=>$e->errorInfo[2]]);
        }catch(Exception $e){
            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

    }

    
    
}
