<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class DashboardController extends Controller
{
    //

    public function __construct(){

        $this->middleware('auth');

    }

    public function index(){

        $user = Auth::user();
       
        $route = $this->getAuthUserDashboard($user);
       

        return redirect()->route($route);
    }

    public function getAuthUserDashboard(User $user){

        switch($user->role){

            case $user->isAdmin():
                $route = 'admin.dashboard';
                break;

            case $user->isSuscriber():
                $route = 'suscriber.dashboard';
                break;

            case $user->isUser():
                $route = 'user.dashboard';
                break;

            default:
                $route = 'home';
        }

        return $route;
    }
}
