<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Unicodeveloper\Paystack\Facades\Paystack;
use GuzzleHttp\Exception\ConnectException;
use Cart;
use App\OrderItem;
use App\Product;
use League\Flysystem\Exception;
use GuzzleHttp\Exception\ClientException;
use App\Order;

class PaymentController extends Controller
{
    //
     /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()

    {
        try {

           return  Paystack::getAuthorizationUrl()->redirectNow();

        }catch(ConnectException $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }catch(ClientException $e){
             return redirect()->back()->with(['error'=>$e->getMessage()]);
        }catch(Exception $e){
             return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

      
       
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        try {
            

            $paymentDetails = Paystack::getPaymentData();
            
            if($paymentDetails['data']['status'] == true){
                
                $order = $paymentDetails['data']['metadata']['orderID'];
                $this->storeOrderItems($order);
                Cart::destroy();
                return  redirect()->route('order_success',['order'=>$order]);
                
            }
            
            throw new \Exception("Order was not successful");

        }catch(Exception $e){
            return redirect()->route('order_error')->with(['erorr'=>$e->getMessage()]);
        }
       

        
    }

    public function storeOrderItems($orderID){

        $order = Order::find($orderID);

        foreach(Cart::content() as $cartItem){
            $orderItem = new OrderItem();
            $orderItem->order_id = $order->id;
            $orderItem->product_id = $cartItem->id;
            $orderItem->price = $cartItem->price;
            $orderItem->save();
        }
        
    }
}
