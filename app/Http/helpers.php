<?php

if(! function_exists('getMoneyEquivalent')){

     function getMoneyEquivalent($eggshellpoint){

        return (int)$eggshellpoint * 0.335;
     }
}

if(! function_exists('getOrderStatusMessage')){

    function getOrderStatusMessage($status_number){
        $msg = null;
        switch($status_number){
            case 1 :
                $msg = 'Pending';
                break;
            case 2 :
                $msg = 'Success';
                break;
            case 3 :
                $msg = 'cancelled';
                break;
            default:
                $msg = 'Invalid';
            
        }

        return $msg;
    }
}
?>