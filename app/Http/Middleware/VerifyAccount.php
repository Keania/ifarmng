<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(isset($user->token) && is_null($user->email_verified_at)){
            
            return redirect()->back()->with(['error'=>' Your Account is not verified You cannot access this link']);
        }
        return $next($request);
    }
}
