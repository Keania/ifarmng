<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuscriberOrder extends Model
{
    //
    protected $guarded = [];

    public function scopePending($query)
    {
        return $query->where('status',1);
    }

    public function scopeSuccess($query)
    {
        return $query->where('status',2);
    }

    public function scopeCancelled($query)
    {
        return $query->where('status',3);
    }

   
}
