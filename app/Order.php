<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\OrderItem;
use App\User;

class Order extends Model
{
    //

    protected $guarded = [];

    public function scopeFulfilled($query)
    {
        return $query->where('status','=',2);
    }

    public function scopeUnFulfilled($query)
    {
        return $query->where('status','=',1);
    }

    public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }
    
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopePending($query)
    {
        return $query->where('status',1);
    }

    public function scopeSuccess($query)
    {
        return $query->where('status',2);
    }

    public function scopeCancelled($query)
    {
        return $query->where('status',3);
    }
}
