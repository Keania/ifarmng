<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/verification/email',function(){
    return view('verify_email');
});

Route::post('/sendcontact/mail','FrontController@sendContactMail')->name('contact_mail');

Route::get('/resend/verification/link','User\UserDashboardController@resendLink')->name('resend.verification');




    Route::get('/cart','CartController@index')->name('cart.index');
    Route::post('/cart/add','CartController@add')->name('cart.add');
    Route::post('/cart/update','CartController@update')->name('cart.update');
    Route::post('/cart/remove','CartController@remove')->name('cart.remove');
    Route::post('/cart/destroy','CartController@destroy')->name('cart.destroy');
    
    // this groups route can only be accessed by verified users
    Route::group(['middleware'=>['auth','email_verified']],function(){
        
        Route::get('/review/order','OrderController@review')->name('order.review');
        Route::get('/create/order','OrderController@createOrder')->name('order.create');
        Route::get('/success/order','OrderController@success')->name('order_success');
        Route::get('/error/order','OrderController@error')->name('order_error');
        Route::get('/cancel/order/{id}','OrderController@cancel')->name('cancel.order');
        Route::post('/pay','PaymentController@redirectToGateway')->name('pay');
        
        Route::post('/dashboard','Suscriber\SuscriberDashboardController@saveCashoutRequest')->name('claim.cash.offer');
        Route::get('/pay/point/review/{id}','Suscriber\SuscriberDashboardController@reviewPayWithPoint')->name('pay.with.points');
        Route::post('/order/offer','Suscriber\SuscriberDashboardController@saveOfferRequest')->name('store.offer.order');
    });

   




Route::get('/verify/email/{token}','FrontController@verifyEmail')->name('verify.email');

Route::get('/','FrontController@index')->name('home');
Route::get('/gallery','FrontController@gallery')->name('gallery');
Route::get('/suscribers/offers','FrontController@offers')->name('suscribers_offers')->middleware('auth');
Route::get('/review/suscribers/offer/{id}','FrontController@reviewOffer')->name('review.offer');
Route::get('/blog','FrontController@blog_index')->name('blog.index');
Route::get('/shop','FrontController@shop_index')->name('shop.index');
Route::get('/shop/{product_id}','FrontController@shop_single')->name('shop.single');
Route::get('/about','FrontController@about')->name('about');
Route::get('/contact','FrontController@contact')->name('contact');
Route::get('/post/{id}','FrontController@post')->name('post.single');
Route::get('/register/suscriber','Auth\RegisterController@showSuscriberRegisterForm')->name('suscriber.register.form');
Route::get('/register/volunteer','Volunteer\RegisterController@showRegistrationForm')->name('volunteer.register.form');
Route::post('/register/volunteer','Volunteer\RegisterController@register')->name('volunteer.register');



Route::post('/order/status/success/{id}','OrderController@markAsSuccessOrder')->name('mark.order.success');
Route::post('/order/status/decline/{id}','OrderController@markAsDeclineOrder')->name('mark.order.decline');

Route::post('/suscriberOrder/status/success/{id}','OrderController@markAsSuccessSuscriberOrder')->name('mark.suscriberOrder.success');
Route::post('/suscriberOrder/status/decline/{id}','OrderController@markAsDeclineSuscriberOrder')->name('mark.suscriberOrder.decline');

Route::post('/order/cashRequest/status/success/{id}','OrderController@markAsSuccessCashRequest')->name('mark.cashRequest.success');
Route::post('/order/cashRequest/status/decline/{id}','OrderController@markAsDeclineCashRequest')->name('mark.cashRequest.decline');

Route::get('/pay/callback','PaymentController@handleGatewayCallback')->name('pay.gateway.callback');



Route::group(['middleware'=>'auth'],function(){

    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    

    Route::group(['prefix'=>'admin'],function(){

        Route::get('/dashboard','Admin\AdminDashboardController@index')->name('admin.dashboard');
        Route::get('/add/product','ProductController@add')->name('admin.add.product');
        Route::post('/update/product/','ProductController@updateProduct')->name('admin.update.product');
        Route::get('/view/products','ProductController@index')->name('admin.view.products');

        Route::get('/add/post','BlogController@add')->name('admin.add.post');
        Route::get('/view/posts','BlogController@index')->name('admin.view.posts');

        Route::post('/delete/product','ProductController@delete')->name('delete.product');
        Route::post('/add/product','ProductController@addProduct')->name('add.product');

        Route::get('/view/suscribe_products','SuscriberProductController@index')->name('admin.suscriber_products');
        Route::post('/delete/suscribe_product/{id}','SuscriberProductController@delete')->name('admin.delete.suscriber_product');
        Route::post('/add/suscribe_product','SuscriberProductController@addProduct')->name('admin.add.suscriber_product');
        Route::post('/update/suscribe_product','SuscriberProductController@updateProduct')->name('admin.update.suscriber_product');
        Route::get('/add/suscriber_product/get','SuscriberProductController@addOfferForm')->name('get.suscriber.offer.form');

        Route::post('/add/post','BlogController@addPost')->name('add.post');
        Route::post('/update/post','BlogController@updatePost')->name('update.post');
        Route::post('/delete/post','BlogController@delete')->name('delete.post');

        Route::get('/orders','Admin\AdminDashboardController@productOrders')->name('admin.product.orders');
        Route::get('/orders/cashRequest','Admin\AdminDashboardController@cashRequestOrders')->name('admin.cash.request');
        Route::get('/orders/sucribers','Admin\AdminDashboardController@suscriptionProductOrders')->name('admin.suscription.orders');
        Route::post('/order/fulfill/{id}','Admin\AdminDashboardController@markOrderAsFulfilled')->name('fulfill.order');
        Route::get('/order/preview/{id}','Admin\AdminDashboardController@previewOrder')->name('preview.order');

        Route::get('/all/users','Admin\AdminDashboardController@users')->name('all.users');
        Route::get('/all/volunteers','Admin\AdminDashboardController@volunteers')->name('all.volunteers');
        Route::get('/all/suscribers','Admin\AdminDashboardController@suscribers')->name('all.suscribers');
        
        Route::get('/add/shell/points/{id}','Admin\AdminDashboardController@addShellPoints')->name('add.shell.points');
        Route::post('/add/shell/points','Admin\AdminDashboardController@addShellPointsPost')->name('add.shell.points.post');
        
    });

    Route::group(['prefix'=>'user'],function(){

        Route::get('/dashboard','User\UserDashboardController@index')->name('user.dashboard');
        Route::post('/update/profile','User\UserDashboardController@updateProfile')->name('update.profile.info');
        
    });

    Route::group(['prefix'=>'suscriber'],function(){

        Route::get('/dashboard','Suscriber\SuscriberDashboardController@index')->name('suscriber.dashboard');
       

    });
});


Auth::routes();


