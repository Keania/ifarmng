<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Verification Email</title>
    <style>
            header{
                width:96%;
                margin:auto;
                height:80px;
              
                padding:2%;
              
            }

            section#container {
                width:75%;
                margin:auto;
                font-family:cursive;
                font-size: 1rem;
            }
            span#slogan {
                float:right;
                color:white;
            }
            p#salutation {
                font-weight:bold;
            }
            p#verifyButton {
                text-align:center;
            }
            #verifyButton button {
                background-color:#a8cf45;
                color:white;
                font-size:1.1rem;
                padding:10px;
                border:none;
                outline:none;
                border-radius:5px;
            }
            #verifyButton a {
                color:white;
                text-decoration:none;
            }
            p {
                margin-bottom:2rem;
            }
    
    </style>
</head>
<body>

    <section id="container">

        <header>
            <span id="slogan" style="color:black;">Recycle don't toss ....</span>
            <img src="{{asset('images/logo.png')}}">
        </header>
        <section>
            <p>{{$sender_message}}</p>




        </section>
        <footer>
            Thank you.<br>
            Ifarm Team.<br>
            Recycling Nigeria for good.<br>
            ----------------------------------------------------<br>
            #20 Ada-George Road, Port Harcourt, Rivers State, Nigeria.<br>
            ----------------------------------------------------<br>
            Contact us on info@ifarmng.com<br>
            ----------------------------------------------------<br>

        </footer>
    </section>
    
</body>
</html>