@extends('layouts.app')
@section('title')
User - Dashboard
@endsection
@section('content')
@php
    $user = Auth::user();
   
@endphp
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Dashboard</h1>
                </div><!-- .col -->
            </div><!-- .row -->
            
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="news-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-12">
                    <div class="tabs">
                        <ul class="tabs-nav d-flex">
                            
                            <li class="tab-nav d-flex justify-content-center align-items-center" data-target="#tab_2">My Profile</li>
                            <li class="tab-nav d-flex justify-content-center align-items-center" data-target="#tab_3">My Orders</li>
                    
                        </ul>

                        <div class="tabs-container">
                            

                            <div id="tab_2" class="tab-content">
                                <div class="">
                                    <h2>Profile</h2>
                            
                                   
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Name:</span>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <span>{{$user->name}}</span>
                                        
                                        </div>
                                    
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Email:</span>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <span>{{$user->email}}</span>
                                        
                                        </div>
                                    
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Phone:</span>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <span>{{$user->phone}}</span>
                                        
                                        </div>
                                    
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Address:</span>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <span>{{$user->address}}</span>
                                        
                                        </div>
                                    
                                    </div>

                                    
                                    <br>
                                    <br>

                                    <div>
                                        <button  class="btn gradient-bg mr-2" data-toggle="modal" data-target="#editProfile">Edit Profile</button>
                                    
                                    </div>

                                    <div class="modal fade" id="editProfile" tabindex="-1" role="dialog" aria-labelledby="addPoint" aria-hidden="true" data-backdrop="false">

                                        <div class="modal-dialog " role="document">

                                            <div class="modal-content card">

                                                <div class="modal-header card-header">
                                                    <h5 class="modal-title" id="editModalLabel">Edit Profile</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>

                                                <div class="modal-body card-body">

                                                    <form id="editProfileForm" action="{{route('update.profile.info')}}" method="POST" class="form-horizontal">
                                                        {{csrf_field()}}
                                                        <div class="form-group">
                                                            <label class="form-control-label">Name</label>
                                                            <input type="text"class="form-control" name="name" value="{{$user->name}}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="form-control-label">Phone Number</label>
                                                            <input type="text"class="form-control" name="phone" value="{{$user->phone}}">
                                                        
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="form-control-label">Address</label>
                                                            <input type="text" class="form-control" name="address" value="{{$user->address}}">
                                                        
                                                        </div>

                                                       
                                                    
                                                    </form>
                                                
                                                </div>

                                                <div class="modal-footer card-footer">

                                                    <button type="button" class="btn " data-dismiss="modal">Cancel</button>
                                                    <button type="button" onclick="document.getElementById('editProfileForm').submit();"class="btn wg-colored-link">Update Info</button>
                                                </div>
                                            
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                        

                                    

                                   

                                </div><!-- .col -->

                            </div>

                            <div id="tab_3" class="tab-content">
                                @php
                                    $counter = 0;
                                @endphp
                               

                              

                                @if($user->orders->isNotEmpty())

                                    <div class="order-tables">
                                        <h5> Product Orders</h5>

                                        @foreach($user->orders as $order)
                                            <h6>OrderID : # {{$order->ref}} <span class="badge badge-secondary">{{getOrderStatusMessage($order->status)}}</span></h6>
                                            
                                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                             
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Date</th>
                                                    
                                                    <th>Product</th>
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach($order->orderItems as $orderItem)
                                                    @php
                                                        
                                                        $product = App\Product::find($orderItem->product_id);
                                                    @endphp
                                                    <tr>
                                                        <td>
                                                            {{++$counter}}
                                                        </td>
                                                        <td>
                                                            {{date('d/ m /Y',strtotime($orderItem->created_at))}}
                                                        </td>
                                                        <td colspan=2>
                                                            <div>
                                                                <img src="{{asset('storage/products/'.$product->photo)}}" width="100">
                                                                
                                                            </div>
                                                            <div style="color:#A8CF45;font-weight:bold;">
                                                                {{$product->name}}
                                                            </div>
                                                            
                                                           
                                                        
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @endforeach
                                    </div>

                                @endif

                               
                            
                            </div>

                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

@endsection

