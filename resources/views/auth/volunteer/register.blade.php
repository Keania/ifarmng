@extends('layouts.app')

@section('title')
Ifarm - Volunteer Sign Up
@endsection

@section('styles')
    <style>
            #become-volunteer {
                color:#929191;
                margin-top:3rem;
                margin-bottom: 3rem;
            }
            #become-volunteer h3,h4 {
                color:black;
                padding-top:1rem;
                padding-bottom: 1rem;
               
            }

            .btn-ifarm {
                background-color: #A8CF45;
            }
           
    
    </style>
@endsection
@section('content')
    <div id="become-volunteer" class="container">
        <h3>Become a Volunteer</h3>

        <p>Join Ifarm Recyclers to make positive impact on our environment</p>
        <p>
            Ifarm Recyclers draw her curtain to Volunteers across Nigeria in impacting in our community/environment to be cleaner

        </p>
        <p>
            Sign Up below to partner with ifarm recyclers to create a clean and healthy environment.
        </p>

        <div id="volunteer-signup-form">
            <h4 style="margin-bottom:3rem;border-bottom:1px solid  #A8CF45;padding-bottom:2rem;">Volunteer Sign Up Form</h4>

            <form method="POST" method="{{route('volunteer.register')}}">

                {{csrf_field()}}

                <div class="form-group row">
                    <label for="name" class="col-form-label col-md-4">Volunteer Name</label>

                    <div class="col-md-4">
                        <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}">

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    
                </div>

                <div class="form-group row">
                    <label for="phone" class="col-form-label col-md-4">Volunteer Phone</label>

                    <div class="col-md-4">
                        <input type="text" name="phone" id="phone" value="{{old('phone')}}" class="form-control {{$errors->has('phone')? 'is-invalid': ''}}">

                        @if($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('phone')}}</strong>
                            </span>
                        @endif
                    </div>
                   
                </div>

                <div class="form-group row">

                    <label for="email" class="col-form-label col-md-4">Volunteer Email</label>

                    <div class="col-md-4">
                        <input type="text" name="email" id="email"value="{{old('email')}}" class="form-control {{$errors->has('email')? 'is-invalid' : ''}}">
                        @if($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('email')}}</strong>
                            </span>
                        @endif
                    </div>
                   
                </div>

                <div class="form-group row">
                    <label for="current_location" class="col-form-label col-md-4">Volunteer current location</label>
                    <div class="col-md-4">
                        <textarea name="current_location" id="_current_location"value="{{old('current_location')}}" class="form-control {{$errors->has('_current_location')? 'is-invalid' : ''}}"></textarea>
                        @if($errors->has('current_location'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('current_location')}}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="service_type" class="col-form-label col-md-4">Choose service type</label>

                    <div class="col-md-4">
                        <select name="service_type" id="service_type" value="{{old('service_type')}}"class="form-control {{$errors->has('service_type') ? 'is-invalid' : ''}}">
                            <option value="door to door pickup">Door to Door Pickup Assistance</option>
                            <option value="drop off pickup">Drop Off Assistance</option>
                            <option value="pickup point establishment">New PickUp Point Establishment</option>
                        </select>
                        @if($errors->has('service_type'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('service_type')}}</strong>
                            </span>
                        @endif
                    </div>
                   
                   
                </div>


                <div class="form-group row">
                    <label for="duration_of_service" class="form-col-label col-md-4">Duration Of Service</label>

                    <div class="col-md-4">
                        <select name="duration_of_service" id="duration_of_service" value="{{old('duration_of_service')}}" class="form-control {{$errors->has('duration_of_service') ? 'is-invalid':''}}">
                            <option value="1 month">1 month</option>
                            <option value="3-4 months"> 3-4 months</option>
                            <option value="5-6 months"> 5 - 6 months</option>
                            <option value="6months - 1year"> 6 months - 1year</option>
                            <option value="1 year & above">1 year & Above</option>
                        </select>
                        @if($errors->has('duration_of_service'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('duration_of_service')}}</strong>
                            </span>
                        @endif

                    </div>
                   
                </div>

                <div class="form-group row">
                    <label for="availability" class="form-col-label col-md-4">Availability</label>

                    <div class="col-md-4">
                        <select name="availability" id="availability" value="{{old('availability')}}" class="form-control {{$errors->has('availability') ? 'is-invalid' : ''}}">
                            <option value="Ready to start">I'm Ready to start</option>
                            <option value="Always Available"> Always Available</option>
                            <option value="confirm availability">Call to confirm availability</option>
                        </select>
                        @if($errors->has('availability'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('availability')}}</strong>
                            </span>
                        @endif

                    </div>
                    
                </div>

                <div class="form-group row">
                    <label for="hear_about_us" class="col-form-label col-md-4">How did you hear about us</label>

                    <div class="col-md-4">
                        <select name="hear_about_us" id="hear_about_us" class="form-control {{$errors->has('hear_about_us') ? 'is-invalid' : ''}}">
                            <option value="social Media">Social Media</option>
                            <option value="Magazine">Magazine</option>
                            <option value="Website">Website</option>
                            <option value="Colleagues">Colleagues</option>
                            <option value="Group Chat">Group Chat</option>
                        </select>
                        @if($errors->has('availability'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('availability')}}</strong>
                            </span>
                        @endif
                        
                    </div>
                   
                </div>

                <div class="form-group">
                    <input type="submit" name="submit" value="Submit" class=" btn btn-ifarm">
                </div>

                       
                       
                       


            </form>
        </div>
    </div>

@endsection