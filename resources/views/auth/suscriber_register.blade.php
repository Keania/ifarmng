@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12 wg-height-30px">
        </div>
        <div class="col-md-8" style="margin-top:4rem;margin-bottom:2.5rem;">
            <div class="">
                <div ><h3 style="text-align:center;margin-bottom:3rem;border-bottom:1px solid  #A8CF45;padding-bottom:2rem;">{{ __('Register as Suscriber') }}</h3></div>

                <div class="">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <input type="hidden" name="role" value="2">

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        <div class="form-group row">
                        
                             <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>   

                             <div class="col-md-6">
                                <input id="phone" type="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        
                        </div>

                        <div class="form-group row">
                        
                            <label for="suscription_type" class="col-md-4 col-form-label text-md-right">{{ __('Suscription Type') }}</label>   

                        <div class="col-md-6">
                           <select id="suscription_type" class="form-control{{ $errors->has('suscription_type') ? ' is-invalid' : '' }}" name="suscription_type" value="{{ old('suscription_type') }}" required>
                                <option value="door to door"> Door to Door Iniative</option>
                                <option value="drop off">Drop off Iniative</option>
                           </select>

                           @if ($errors->has('suscription_type'))
                               <span class="invalid-feedback" role="alert">
                                   <strong>{{ $errors->first('suscription_type') }}</strong>
                               </span>
                           @endif
                       </div> 
                   
                        </div>

                        <div class="form-group row">

                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Current Location Address') }}</label>
                        
                                   
                            <div class="col-md-6">
                                <textarea id="address" type="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required></textarea>

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div> 
                           
                        </div>

                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4" style="margin-bottom:0.9rem;">
                                <a class="" href="{{ route('login') }}" style="color:blue;text-decoration:underline;">
                                        {{ __('Already has an Account? Login') }}
                                </a>
                            </div>
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-ifarm">
                                    {{ __('Register') }}
                                </button>
                               
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 wg-height-100px">
        </div>
    </div>
</div>
@endsection
