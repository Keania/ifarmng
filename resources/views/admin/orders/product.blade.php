@extends('layouts.admin.app')
@section('title')
Ifarm - Users
@endsection

@section('styles')
    <link rel="stylesheet" href="{{asset('admin/vendors/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/themify-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/selectFX/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">


    <link rel="stylesheet" href="{{asset('admin/assets/css/style.css')}}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
@endsection

@section('contents')
<div id="right-panel" class="right-panel">

<!-- Header-->
<!-- <header id="header" class="header">

    <div class="header-menu">

        <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
            <div class="header-left">
                <button class="search-trigger"><i class="fa fa-search"></i></button>
                <div class="form-inline">
                    <form class="search-form">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                        <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                    </form>
                </div>

                <div class="dropdown for-notification">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        <span class="count bg-danger">5</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="notification">
                        <p class="red">You have 3 Notification</p>
                        <a class="dropdown-item media bg-flat-color-1" href="#">
                        <i class="fa fa-check"></i>
                        <p>Server #1 overloaded.</p>
                    </a>
                        <a class="dropdown-item media bg-flat-color-4" href="#">
                        <i class="fa fa-info"></i>
                        <p>Server #2 overloaded.</p>
                    </a>
                        <a class="dropdown-item media bg-flat-color-5" href="#">
                        <i class="fa fa-warning"></i>
                        <p>Server #3 overloaded.</p>
                    </a>
                    </div>
                </div>

                <div class="dropdown for-message">
                    <button class="btn btn-secondary dropdown-toggle" type="button"
                        id="message"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-email"></i>
                        <span class="count bg-primary">9</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="message">
                        <p class="red">You have 4 Mails</p>
                        <a class="dropdown-item media bg-flat-color-1" href="#">
                        <span class="photo media-left"><img alt="avatar" src="images/avatar/1.jpg"></span>
                        <span class="message media-body">
                            <span class="name float-left">Jonathan Smith</span>
                            <span class="time float-right">Just now</span>
                                <p>Hello, this is an example msg</p>
                        </span>
                    </a>
                        <a class="dropdown-item media bg-flat-color-4" href="#">
                        <span class="photo media-left"><img alt="avatar" src="images/avatar/2.jpg"></span>
                        <span class="message media-body">
                            <span class="name float-left">Jack Sanders</span>
                            <span class="time float-right">5 minutes ago</span>
                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                        </span>
                    </a>
                        <a class="dropdown-item media bg-flat-color-5" href="#">
                        <span class="photo media-left"><img alt="avatar" src="images/avatar/3.jpg"></span>
                        <span class="message media-body">
                            <span class="name float-left">Cheryl Wheeler</span>
                            <span class="time float-right">10 minutes ago</span>
                                <p>Hello, this is an example msg</p>
                        </span>
                    </a>
                        <a class="dropdown-item media bg-flat-color-3" href="#">
                        <span class="photo media-left"><img alt="avatar" src="images/avatar/4.jpg"></span>
                        <span class="message media-body">
                            <span class="name float-left">Rachel Santos</span>
                            <span class="time float-right">15 minutes ago</span>
                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                        </span>
                    </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-5">
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                </a>

                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="#"><i class="fa fa-user"></i> My Profile</a>

                    <a class="nav-link" href="#"><i class="fa fa-user"></i> Notifications <span class="count">13</span></a>

                    <a class="nav-link" href="#"><i class="fa fa-cog"></i> Settings</a>

                    <a class="nav-link" href="#"><i class="fa fa-power-off"></i> Logout</a>
                </div>
            </div>

            <div class="language-select dropdown" id="language-select">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                    <i class="flag-icon flag-icon-us"></i>
                </a>
                <div class="dropdown-menu" aria-labelledby="language">
                    <div class="dropdown-item">
                        <span class="flag-icon flag-icon-fr"></span>
                    </div>
                    <div class="dropdown-item">
                        <i class="flag-icon flag-icon-es"></i>
                    </div>
                    <div class="dropdown-item">
                        <i class="flag-icon flag-icon-us"></i>
                    </div>
                    <div class="dropdown-item">
                        <i class="flag-icon flag-icon-it"></i>
                    </div>
                </div>
            </div>

        </div>
    </div>

</header> -->
<!-- Header-->

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        @if(session()->has('success'))
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session()->get('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif(session()->has('error'))

            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Error</span> {{session()->get('error')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

        @endif
    </div>
</div>

<div class="content mt-3">
   
    <div class="animated fadeIn">
        <div class="row">


            @if(count(App\Order::all()) > 0)
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Product Orders</strong>
                        </div>
                        <div class="card-body">

                            <h5>Pending Orders</h5>
                            <table  class="table table-striped table-bordered datatables" >
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Order Reference</th>
                                        <th>Amount</th>
    
                                        <th>Action</th>
                                        <th>More Details</th>

                                    
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach(App\Order::pending()->get() as $pending)
                                        <tr>
                                            <td>
                                                {{$pending->user->name}}
                                            </td>
                                            <td>
                                                {{$pending->ref}}
                                            </td>
                                            <td>
                                                {{$pending->amount}}
                                            </td>
                                            <td>
                                                 <div class="dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Take Action
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item" href="{{route('mark.order.success',['id'=>$pending->id])}}" onclick="event.preventDefault();document.getElementById('deliveredPending{{$pending->id}}').submit();">Accept</a>
                                                        <a class="dropdown-item" href="{{route('mark.order.decline',['id'=>$pending->id])}}" onclick="event.preventDefault();document.getElementById('declinePending{{$pending->id}}').submit();">Declined</a>
                                                    
                                                    </div>

                                                    <form id="deliveredPending{{$pending->id}}" method="POST" action="{{route('mark.order.success',['id'=>$pending->id])}}" style="display:none;">
                                                        @csrf
                                                    </form>
                                                    <form id="declinePending{{$pending->id}}" method="POST" action="{{route('mark.order.decline',['id'=>$pending->id])}}" style="display:none;">
                                                        @csrf
                                                    </form>
                                                </div>
                                                
                                                
                                            </td>
                                            <td>
                                                <button data-toggle="modal" data-target="#modal{{$pending->id}}" class="btn btn-sm btn-primary">View User Details</button>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade" id="modal{{$pending->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table>
                                                                    <tbody>
                                                                        
                                                                        <tr>
                                                                            <td>Phone</td>
                                                                            <td>{{$pending->user->phone}}</td>
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                            <td>Email</td>
                                                                            <td>{{$pending->user->email}}</td>
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                            <td>Address</td>
                                                                            <td>{{$pending->user->address}}</td>
                                                                        </tr>
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            
                                           
                                        </tr>
                                    @endforeach

                                </tbody>
                               
                               
                            </table>

                            <h5>Paid Orders</h5>
                            <table  class="table table-striped table-bordered datatables">
                                 <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Order Reference</th>
                                        <th>Amount</th>
    
                                        <th>Action</th>
                                       
                                        <th>More Details</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach(App\Order::success()->get() as $success)
                                        <tr>
                                            <td>
                                                {{$success->user->name}}
                                            </td>
                                            <td>
                                                {{$success->ref}}
                                            </td>
                                            <td>
                                                {{$success->amount}}
                                            </td>
                                            <td>
                                                No Action Available
                                            </td>
                                             <td>
                                                <button data-toggle="modal" data-target="#modal{{$success->id}}" class="btn btn-sm btn-primary">View User Details</button>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade" id="modal{{$success->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table>
                                                                    <tbody>
                                                                        
                                                                        <tr>
                                                                            <td>Phone</td>
                                                                            <td>{{$success->user->phone}}</td>
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                            <td>Email</td>
                                                                            <td>{{$success->user->email}}</td>
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                            <td>Address</td>
                                                                            <td>{{$success->user->address}}</td>
                                                                        </tr>
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                           
                                        </tr>
                                    @endforeach

                                </tbody>
                               
                               
                            </table>

                            <h5>Cancelled Orders</h5>
                            <table  class="table table-striped table-bordered datatables">
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Order Reference</th>
                                        <th>Amount</th>
    
                                        <th>Action</th>
                                        <th>View Details</th>
                                    
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach(App\Order::cancelled()->get() as $cancelled)
                                        <tr>
                                            <td>
                                                {{$cancelled->user->name}}
                                            </td>
                                            <td>
                                                {{$cancelled->ref}}
                                            </td>
                                            <td>
                                                {{$cancelled->amount}}
                                            </td>
                                            <td>
                                                
                                                <form method="POST" action="{{route('mark.order.success',['id'=>$cancelled->id])}}">
                                                    @csrf
                                                    <button type="submit" name="submit" class="btn btn-success">Accept</button>
                                                </form>
                                            </td>
                                             <td>
                                                <button data-toggle="modal" data-target="#modal{{$cancelled->id}}" class="btn btn-sm btn-primary">View User Details</button>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade" id="modal{{$cancelled->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table>
                                                                    <tbody>
                                                                        
                                                                        <tr>
                                                                            <td>Phone</td>
                                                                            <td>{{$cancelled->user->phone}}</td>
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                            <td>Email</td>
                                                                            <td>{{$cancelled->user->email}}</td>
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                            <td>Address</td>
                                                                            <td>{{$cancelled->user->address}}</td>
                                                                        </tr>
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                           
                                        </tr>
                                    @endforeach

                                </tbody>
                               
                               
                            </table>

                        </div>
                    </div>
                </div>
            @else 
                <div class="col-md-12">
                    <h5>No suscription Orders is Available Yet</h5>
                </div>
            @endif


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<style>
    
    .modal-backdrop {
        display :none;
    }
</style>
@endsection

@section('scripts')
    <script src="{{asset('admin/vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/vendors/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('admin/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/main.js')}}"></script>

    <script src="{{asset('admin/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('admin/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('admin/vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/init-scripts/data-table/datatables-init.js')}}"></script>
    <script>

        $('#all__users__table').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
    
    
    
    </script>
    
@endsection




 