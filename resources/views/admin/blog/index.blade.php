@extends('layouts.admin.app')
@section('title')
Products
@endsection

@section('styles')
    <link rel="stylesheet" href="{{asset('admin/vendors/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/themify-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/selectFX/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">


    <link rel="stylesheet" href="{{asset('admin/assets/css/style.css')}}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
@endsection

@section('contents')
<div id="right-panel" class="right-panel">

<!-- Header-->
<!-- <header id="header" class="header">

    <div class="header-menu">

        <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
            <div class="header-left">
                <button class="search-trigger"><i class="fa fa-search"></i></button>
                <div class="form-inline">
                    <form class="search-form">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                        <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                    </form>
                </div>

               
            </div>
        </div>

        
    </div>

</header> -->
<!-- Header-->

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        @if(session()->has('success'))
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session()->get('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif(session()->has('error'))

            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Error</span> {{session()->get('error')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

        @endif
    </div>
</div>

<div class="content mt-3">
   
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Product Listings</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Post</th>
                                    <th>Contents</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $post)
                                    <tr>
                                        <td>

                                            <img src="{{asset('storage/posts/'.$post->image_file)}}" width="100" height="100">
                                            <div>{{$post->post_title}}</div>
                                        
                                        </td>
                                        
                                        <td>
                                            {{$post->post_content}}
                                        
                                        </td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#edit{{$post->id}}">
                                                <i class="fa fa-edit fa-lg"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{route('delete.post',['post_id'=>$post->id])}}" onclick="event.preventDefault();document.getElementById('delete{{$post->id}}').submit();">
                                                <i class="fa fa-trash-o fa-lg"></i>
                                            </a>
                                            <form id="delete{{$post->id}}" method="post" action="{{route('delete.post')}}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="post_id" value="{{$post->id}}">
                                            
                                            </form>
                                        
                                        </td>
                                        <td>

                                        <div class="modal fade" id="edit{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true" data-backdrop="false">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="mediumModalLabel">Edit {{$post->post_title}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{route('update.post')}}" method="post" novalidate="novalidate" enctype="multipart/form-data">

                                                        {{csrf_field()}}
                                                        <input type="hidden" name="post_id" value="{{$post->id}}">
                                                        <div class="form-group">
                                                            <label for="post_title" class="control-label mb-1">Post Title</label>
                                                            <input id="post_title" name="post_title" type="text" class="form-control" aria-required="true" aria-invalid="false" value="{{$post->post_title}}">
                                                        </div>



                                                        <div class="form-group">

                                                            <label for="post_photo" class="control-label mb-1">Photo</label>
                                                            <input type="file" name="post_photo" class="form-control">
                                                            

                                                        </div>

                                                        <div class="form-group">

                                                            <label for="post_content" class="control-label mb-1">Post</label>
                                                            <textarea name="post_content" class="form-control" rows="9">{{$post->post_content}}</textarea>


                                                        </div>
                                                        
                                                            
                                                        <div>
                                                            <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                                                <i class="fa fa-cloud-upload fa-lg"></i>&nbsp;
                                                                <span id="payment-button-amount">Upload</span>
                                                                <span id="payment-button-sending" style="display:none;">Sending…</span>
                                                            </button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                   
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                        <button type="button" class="btn btn-primary">Confirm</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@endsection

@section('scripts')
    <script src="{{asset('admin/vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/vendors/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('admin/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/main.js')}}"></script>

    <script src="{{asset('admin/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('admin/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('admin/vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('admin/vendors/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('js/init-scripts/data-table/datatables-init.js')}}"></script>
    
@endsection




 