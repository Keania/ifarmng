@extends('layouts.admin.app')
@section('title')
Ifarm - Add Points
@endsection

@section('styles')
    <link rel="stylesheet" href="{{asset('admin/vendors/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/themify-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendors/selectFX/css/cs-skin-elastic.css')}}">
    
    <link rel="stylesheet" href="{{asset('admin/assets/css/style.css')}}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <style>
            @media print {
                aside.left-panel {
                   display: none;
                }
                .right-panel {
                    display: block;
                    font-size:1.5rem;
                    margin:auto;
                    color:grey;
                }
                .offset-md-1 {
                    margin:0px;
                }
                div[class*="col-"] {
                    float: none;
                }

                .no-print {
                    display: none;
                }

            }
    
    </style>
@endsection

@section('contents')
<div id="right-panel" class="right-panel">

<!-- Header-->
<header id="header" class="header">

    <div class="header-menu">

      
    </div>

</header><!-- /header -->
<!-- Header-->

<div class="breadcrumbs">
    
    <div class="col-sm-12">
        @if(session()->has('success'))
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session()->get('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif(session()->has('error'))

            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Error</span> {{session()->get('error')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

        @endif
    </div>
</div>

<div class="content mt-3">
   
   <div class="row">

        <div class="col-lg-10 offset-md-1">
            <div class="card">
                <div class="card-header">
                    <img width="199" height="70" class="d-block" src="{{asset('images/logo.png')}}" alt="logo">
                </div>
                <div class="card-body">
                    
                    <div id="pay-invoice">
                        <div class="card-body">
                            <div class="card-title text-center">
                                <h3 >SHELL POINTS CARD</h3>
                                <br>
                                <h5>{{$suscriber->name}}</h5>
                                <h5>{{$suscriber->email}}</h5>
                                <h5>{{$suscriber->phone}}</h5>
                            </div>
                            
                            <hr>
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Description</th>
                                        <th>Weight</th>
                                        <th>Points Obtained</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $counter = 0;
                                    @endphp
                                    @foreach($suscriber->ES_Points as $eggPoint)
                                    @php
                                        $counter++;
                                    @endphp
                                        <tr>
                                            <td>
                                                {{$counter}}
                                            </td>
                                            <td>
                                                {{date('d/ m /Y',strtotime($eggPoint->created_at))}}
                                            </td>
                                            <td>
                                                {{date('H:i A ',strtotime($eggPoint->created_at))}}
                                            </td>
                                            <td>
                                                {{$eggPoint->description}}
                                            
                                            </td>
                                            <td>
                                                {{$eggPoint->weight}}
                                            </td>
                                            <td>
                                                {{$eggPoint->points}}
                                            
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <a href="#" onclick="print();" style="float:right" class="no-print">
                                <i class="fa fa-print"></i>
                            </a>
                        </div>
                    </div>

                </div>
            </div> <!-- .card -->

        </div>
   
   
   </div>
   
</div><!-- .content -->
@endsection

@section('scripts')
    <script src="{{asset('admin/vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/vendors/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('admin/vendors/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('admin/vendors/jquery-validation-unobtrusive/dist/jquery.validate.unobtrusive.min.js')}}"></script>
    <script src="{{asset('admin/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/assets/js/main.js')}}"></script>

    
    
@endsection




 