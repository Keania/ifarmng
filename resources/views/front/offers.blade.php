@extends('layouts.app')
@section('title')
Ifarm - Shop
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection
@section('content')
<div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Suscribers Center</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="news-wrap">
        <div class="container">
            <div class="row">
                @php

                  
                    if(Auth::user()->isDropOff()){
                        $offers = App\SuscribeProduct::latest()->get();
                    }
                @endphp

                @if(isset($offers))

                    @foreach($offers as $offer)
                    
                        <div class="col-md-6 wg-text-center">
                            <div class="wg-store-item-wrapper">
                                <div class="">
                                    <div class="">
                                        <img src="{{asset('storage/suscribe_products/'.$offer->image)}}" class="wg-store-img">
                                    </div>
                                    <div class="">
                                        <h5 class="wg-product-name">{{$offer->name}}</h5>
                                        <p class="wg-product-price">{{$offer->egg_shell_points}}pts
                                        </p>
                                        <div class="">
                                            
                                            <a class="wg-colored-link" href="{{route('review.offer',['offer_id'=>$offer->id])}}" style="font-size:1.3em;">
                                                <i class="fa fa-arrow"> Make Request</i>
                                            </a>
                                        </div>
                                        
                                    </div>

                                    
                                </div>
                            </div>
                            <div class="wg-full-width wg-height-30px"></div>
                        </div>
                    
                    @endforeach

                @elseif(isset(Auth::user()->ESP_Wallet->balance) && Auth::user()->isDoorToDoor())
                
                    <div class="col-md-6">
                        <p>
                            You are suscribed onto the Ifam Door to Door Iniative fill the form below to apply for a cashout 
                        </p>

                        <form method="POST" action="{{route('claim.cash.offer')}}" >
                            {{csrf_field()}}
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="account_name" class="form-col-label">Egg Shell Point Balance</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text"  class="form-control" value="{{Auth::user()->ESP_Wallet->balance}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="account_name" class="form-col-label">Account Name</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="account_name" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="account_number" class="form-col-label">Account Number</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="account_number" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="bank" class="form-col-label">Bank</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="bank" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                               
                                <div class="col-md-8 offset-md-4">
                                    Your egg shell point is valued at N{{getMoneyEquivalent(Auth::user()->ESP_Wallet->balance)}}
                                </div>
                            </div>

                            <div class="form-group row">
                               
                                <div class="col-md-8 offset-md-4">
                                    <input type="submit" name="submit" value="Apply" class="btn gradient-bg mr-2">
                                </div>
                            </div>
                        </form>
                    </div>
                @else 
                    <div class="col-md-6">
                        <p> There is nothing here for you yet</p>
                    </div>

                @endif
               
                  
                   
           


            </div>
        </div>
    </div>
   
@endsection
