@extends('layouts.app')
@section('title')
Ifarm - Contact Us
@endsection
@section('styles')
    <style>
       
    </style>
@endsection
@section('content')
<div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Contact</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="contact-page-wrap">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5">
                    <div class="entry-content">
                        <h2>Get In touch with us</h2>

                        <p>Stay in touch with us</p>

                       

                        <ul class="contact-info p-0">
                            <li><i class="fa fa-phone"></i><span>+234 (0) 7031616551</span></li>
                            <li><i class="fa fa-envelope"></i><span>info@ifarmng.com</span></li>
                            <li><i class="fa fa-map-marker"></i><span>Port Harcourt, Rivers State</span></li>
                        </ul>
                    </div>
                </div><!-- .col -->

                <div class="col-12 col-lg-7">
                    <form class="contact-form" action="{{route('contact_mail')}}" method="POST">
                        {{csrf_field()}}
                        <input type="text" placeholder="Name" name="sender_name" value="{{old('sender_name')}}">
                        <input type="email" placeholder="Email" name="sender_email" value="{{old('sender_email')}}">
                        <textarea rows="15" cols="6" placeholder="Messages" name="sender_message" value="{{old('sender_message')}}"></textarea>

                        <span>
                            <input class="btn gradient-bg" type="submit" value="Contact us">
                        </span>
                    </form><!-- .contact-form -->

                </div><!-- .col -->

              
            </div><!-- .row -->
        </div><!-- .container -->
    </div>
@endsection