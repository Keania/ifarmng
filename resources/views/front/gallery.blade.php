@extends('layouts.app')
@section('title')
    Ifarm-Gallery
@endsection
@section('styles')
    <style>
        .row {
            margin-bottom:1.5rem;
        }
        img.col-sm-12 {
            margin-bottom:0.8rem;
            height:300px;
        }
        
        @media (max-width:800px){
            img.col-sm-12 {
                height:550px;
            }
        }
        
    </style>
@endsection
@section('content')
    <div class="container" style="padding-top:5rem;padding-bottom:4rem;">
         <div class="row" style="text-align:center;">
             <img src="{{asset('images/upload (1).jpg')}}" class="col-sm-12 col-md-6 col-lg-3" >
             <img src="{{asset('images/upload (2).jpg')}}"  class="col-sm-12 col-md-6 col-lg-3">
             <img src="{{asset('images/upload (3).jpg')}}" class="col-sm-12 col-md-6 col-lg-3">
             <img src="{{asset('images/upload (4).jpg')}}"  class="col-sm-12 col-md-6 col-lg-3">
              <img src="{{asset('images/upload (5).jpg')}}"  class="col-sm-12 col-md-6 col-lg-3">
               <img src="{{asset('images/upload (6).jpg')}}"  class="col-sm-12 col-md-6 col-lg-3">
                <img src="{{asset('images/upload (7).jpg')}}"  class="col-sm-12 col-md-6 col-lg-3">
        </div>
        <div class="row">
           
        </div>
            
    </div>
   
@endsection