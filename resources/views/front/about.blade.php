@extends('layouts.app')

@section('title')
Ifarm - About Us
@endsection

@section('styles')
    <style>
        #how-it-works {
            font-size: 14px;
            line-height: 2;
            color: black;
            margin-top:4rem;
        }

        h4,h3,h2,h1 {
            color:#000000b3;
            margin:2rem;
            margin-left:unset;
            font-weight:bold;
            
        }

        .display__none {
            display: none;
        }

        #how-it-works h1 {
            margin-bottom: 4rem;
        }


        #how-it-works h1::before {
            content: '';
            position: relative;
            bottom: 0;
            left: 0;
            width: 64px;
            height: 4px;
            border-radius: 2px;
            background: #A8CF45;
        }
      p {
            margin-top:2rem;
            margin-bottom:2rem;
            font-size:1.1rem;
        }
        @media (min-width:800px){
            #collection_modes {
                display:grid;
                grid-template-columns:1fr 1.3fr;
                grid-column-gap:15px;
            }
        }

        b,h4,h1,h3,h5 {
            color:#A8CF45;
        }
    </style>
@endsection
@section('content')

<div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>About Us</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="welcome-wrap">
        <div class="container">
            <div class="row" style="padding-top:3.5rem;padding-bottom:2rem;">
                <div class="col-12 col-lg-6 order-2 order-lg-1">
                    <div class="welcome-content">
                        <header class="entry-header">
                            <h2 class="entry-title" style=" color:#000000b3;">About Us</h2>
                        </header><!-- .entry-header -->

                        <div class="entry-content mt-5">
                            <p style="color:black;">
                                <b>IFARM</b> is a leading Agricultural waste recycling and social benefit venture in Nigeria that creates value from generated poultry waste.
                                <br>
                                We have created sustainable alternative for handling  poultry waste through recycling them into an eco-friendly product of diverse application.
                            </p>
                        </div><!-- .entry-content -->

                        <div class="entry-footer mt-5">
                            <a href="#" class="btn gradient-bg mr-2"id="read__more__toggler" onclick="event.preventDefault();document.getElementById('how-it-works').classList.toggle('display__none')" style="color:white;">Read More</a>
                        </div><!-- .entry-footer -->
                    </div><!-- .welcome-content -->
                </div><!-- .col -->

                <div class="col-12 col-lg-6 order-1 order-lg-2">
                   <!-- <img src="images/welcome.jpg" alt="welcome" width="100%;"> -->
                </div><!-- .col -->
            </div><!-- .row -->
            <div class="row" >

                <div id="how-it-works" class="col-12 display__none">
                    <h1>How it Works</h1>

                    <div class="ifarm-stages">
                        <h3>Ifarm Recyclers Obtains Eggshells Directly from Post Consumers</h3>

                        <p>Ifarm Recyclers obtains eggshells from  post consumers and this generates a more hygeinic raw material which 
                            adds to a huge value in the recycling process as it reduces the extra work to be done during the processing of eggshells into final product
                        </p>
                        <p>The accumulated eggshells is further processed into a finished raw material with diverse application in the Agricultural sector,
                            Pharmeceutical, Cosmetics, Industrial and Domestic  sector
                        </p>
                    </div>

                    <div class="ifarm-stages">

                        <h3>Mode of Collection</h3>

                        <p style="margin-bottom:0.5rem">
                            <!-- <span style="font-weight:bold;">We practise a door to door and drop off collection of eggshells</span><br> -->
                            <span style="font-weight:bold;color:#000000b5;">At Ifarm we operate two(2) recycling initiatives</span>
                           
                        </p>

                        <ul style="font-size:1rem">
                                <li>Ifarm Recycling door to door</li>
                                <li>Ifarm Recycling drop off</li>
                            </ul>

                       <div id="collection_modes">
                           
                           <div>
                                <h4>Ifarm Recyclers Door to Door</h4>

                                <p>
                                    Suscribers registered under this initiative exchange eggshells for cash rewards according to the accumulated number of Kg after 
                                    eggshells have been weighed. Each Kg of eggshells have a price (contact for pricing). The cash reward is paid into our 
                                    suscribers account.
                                    To signup to our door to door contact us.
                                </p>
                               
                           </div>
                           
                           <div>
                                <h4>Ifarm Recycling Drop off</h4>

                                <p>
                                    Suscribers that are suscribed under the drop off initiative exchange eggshells for a point which can be accumulated over a long period of time.
                                    At drop off point, eggshells brought are being <b>weighed</b> and recorded in kilogrammes. Recorded Kgs are equated to shell points generated internally by 
                                    <b>IFARM RECYCLERS</b> which are then used to redeem valuable items from Ifarm crux .To signup for our drop off iniative <b><a href="{{route('suscriber.register.form')}}">Click here</a> </b>
                                </p>
                               
                           </div>
                       </div>

                       

                       
                        <p style="font-size:1.2rem;">
                            <h5 style="margin-bottom:2rem;">Drop off Center</h5>
                            <b>Port Harcourt Drop off</b> <br> #20 Ada George Road, PHC
                        </p>


                    </div>

                </div>
            </div>
        </div><!-- .container -->
    </div><!-- .home-page-icon-boxes -->

    <!-- <div class="about-stats">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="circular-progress-bar">
                        <div class="circle" id="loader_1">
                            <strong class="d-flex justify-content-center"></strong>
                        </div>

                        <h3 class="entry-title">Hard Work</h3>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="circular-progress-bar">
                        <div class="circle" id="loader_2">
                            <strong class="d-flex justify-content-center"></strong>
                        </div>

                        <h3 class="entry-title">Pure Love</h3>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="circular-progress-bar">
                        <div class="circle" id="loader_3">
                            <strong class="d-flex justify-content-center"></strong>
                        </div>

                        <h3 class="entry-title">Smart Ideas</h3>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="circular-progress-bar">
                        <div class="circle" id="loader_4">
                            <strong class="d-flex justify-content-center"></strong>
                        </div>

                        <h3 class="entry-title">Good Decisions</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-testimonial">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                    <div class="testimonial-cont">
                        <div class="entry-content">
                            <p>We love to help all the children that have problems in the world. After 15 years we have many goals achieved.</p>
                        </div>

                        <div class="entry-footer d-flex flex-wrap align-items-center mt-5">
                            <img src="images/testimonial-1.jpg" alt="">

                            <h4>Maria Williams, <span>Volunteer</span></h4>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 offset-lg-2 col-lg-5">
                    <div class="testimonial-cont">
                        <div class="entry-content">
                            <p>We love to help all the children that have problems in the world. After 15 years we have many goals achieved.</p>
                        </div>

                        <div class="entry-footer d-flex flex-wrap align-items-center mt-5">
                            <img src="images/testimonial-2.jpg" alt="">

                            <h4>Cristian James, <span>Volunteer</span></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- <div class="help-us">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                    <h2>Help us so we can help others</h2>

                    <a class="btn orange-border" href="#">Donate now</a>
                </div>
            </div>
        </div>
    </div> -->
@endsection