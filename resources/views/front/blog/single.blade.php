@extends('layouts.app')
@section('title')
Ifarm - Blog
@endsection

@section('styles')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection
@section('content')

<div class="news-wrapper">

    <div class="container">
    @section('title')
Ifarm - Contact Us
@endsection
        <div class="row">


            <div class="col-md-8 ">
            

                <div class="news-content">

                    <a href="#"><img src="{{asset('storage/posts/'.$post->image_file)}}" alt=""></a>

                    <header class="entry-header d-flex flex-wrap justify-content-between align-items-center">
                        <div class="header-elements">
                            <div class="posted-date">{{date("M d , Y",strtotime($post->created_at))}}</div>

                            <h2 class="entry-title"><a href="#">{{$post->post_title}}</a></h2>

                            
                        </div>

                       
                    </header>

                    <div class="entry-content">
                        <p>{{$post->post_content}}</p>
                    </div>


                </div>
            </div>

            <div class="col-md-3 wg-ml-30px" style="margin-bottom:5em;">
               
                        <div class="popular-posts" style="margin-top:0px;">
                            <h2>Latest Posts</h2>

                           


                            <ul class="p-0" style="margin:30px 0px;">
                                @foreach($latest as $latest_post)
                                    <li class="post-grid">
                                        <figure><a href="{{route('post.single',['id'=>$latest_post->id])}}"><img  class="img-fluid" src="{{asset('storage/posts/'.$latest_post->image_file)}}" alt=""></a></figure>

                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="{{route('post.single',['id'=>$latest_post->id])}}">{{$latest_post->post_title}}</a></h3>

                                            <div class="posted-date">{{date("M d , Y",strtotime($post->created_at))}}</div>
                                        </div>
                                    </li>
                                @endforeach

                                
                            </ul>
                        </div>

                
                   
            
            </div>

            
        
        
        </div>
    
    </div>

</div>
@endsection