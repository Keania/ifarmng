@extends('layouts.app')
@section('title')
Ifarm - Blog
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('css/style.css')}}"> 
@endsection
@section('content')
<div class="page-header wg-dark-background">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Blog</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="news-wrap">
        <div class="container">
            <div class="row">
               
                   
                    @foreach($posts as $post)

                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="cause-wrap">
                            <figure class="m-0">
                            
                                <img src="{{asset('storage/posts/'.$post->image_file)}}" alt="" style="min-height:230px;max-height:230px;">

                               

                                <!-- .figure-overlay -->
                            </figure>

                            <div class="cause-content-wrap">
                                <header class="entry-header d-flex flex-wrap align-items-center">
                                    <h3 class="entry-title w-100 m-0"><a href="#">{{$post->post_title}}</a></h3>
                                </header><!-- .entry-header -->

                                <div class="entry-content">
                                    <p class="m-0">
                                        {{substr($post->post_content,0,30)}}.........
                                    </p>
                                    <br>
                                    <a href="{{route('post.single',['id'=>$post->id])}}" class="btn gradient-bg mr-2" style="color:white;">Read More</a>
                                </div><!-- .entry-content -->

                                
                            </div><!-- .cause-content-wrap -->
                        </div><!-- .cause-wrap -->
                    </div>

                    @endforeach

                    {{$posts->links()}}

                    <!-- <ul class="pagination d-flex flex-wrap align-items-center p-0">
                        <li class="active"><a href="#">01</a></li>
                        <li><a href="#">02</a></li>
                        <li><a href="#">03</a></li>
                    </ul> -->
               

           


            </div>
        </div>
    </div>
    
@endsection

@section('script')

<script>

        $(document).ready(function(){

            let nav = $('ul.pagination')

            console.log(nav)
        });
    
    </script>
@endsection