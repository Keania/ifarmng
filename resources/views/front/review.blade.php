@extends('layouts.app')
@section('title')
    Review Order
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection
@section('content')
<div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Review Order</h1>
                   
                        <h6 style="color:white;">Order ID : #{{$order->ref}}</h6>
                    
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="news-wrap">
        <div class="container">
            <div class="row">

                <!-- <div class="wg-full-width wg-height-100px"></div> -->

                <div class="col-md-10 offset-md-1">

                    <div >
                        <div >
                            @if(count(Cart::content()) > 0)
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Item Name</th>
                                            <th>Item Price</th>
                                            <th>Item Description</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach(Cart::content() as $cartItem)
                                        
                                            <tr>
                                                <td>{{$cartItem->name}}</td>
                                                <td>{{$cartItem->price}}</td>
                                                <td>{{substr($cartItem->options->desc,0,50)}}.....</td>
                                                <td>
                                                    <a href="{{route('cart.remove')}}" onclick="event.preventDefault();document.getElementById('remove{{$cartItem->id}}').submit();"><i class="fa fa-trash fa-lg"></i></a>
                                                    <form id="remove{{$cartItem->id}}" method="POST" action="{{route('cart.remove')}}">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="id" value="{{$cartItem->rowId}}">
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3">Total</th>
                                            <th colspan="3">{{Cart::total()}}</th>
                                        </tr>
                                    </tfoot>
                                </table>

                                

                                <div class="wg-full-width wg-height-30px"></div>
                                <div>
                                   
                                    <a href="{{route('cancel.order',['order'=>$order->id])}}" class="btn btn-default wg-float-right wg-float-right">Cancel Order</a>
                                    <form method="POST" action="{{route('pay')}}" class="wg-float-right">
                                    {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}

                                        <input type="hidden" name="email" value="{{Auth::user()->email ?? 'ifarmng1@gmail.com'}}"> {{-- required --}}
                                        <input type="hidden" name="orderID" value="{{$order->id}}">
                                        <input type="hidden" name="amount" value="{{(int)$order->amount * 100}}"> {{-- required in kobo --}}
                                        <input type="hidden" name="metadata" value="{{json_encode($array = ['orderID'=>$order->id])}}">
                                      
                                      
                                        <input type="hidden" name="reference" value="{{ $order->ref }}"> {{-- required --}}
                                        <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
                                       
                                        <input type="submit" name="submit" class="btn gradient-bg mr-2" value="Pay Now">
                                    </form>
                                </div>
                            @else
                                <div>
                                    <h3>Shopping Cart is empty</h3>
                                </div>
                            @endif
                            
                        </div>
                    </div>

                </div>
               
            </div>
        </div>
    </div>
@endsection
