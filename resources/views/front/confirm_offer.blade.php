@extends('layouts.app')
@section('title')
Ifarm - Suscribr
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection
@section('content')
<div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Shop</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="news-wrap">
        <div class="container">
            <div class="row">
                   
                <div class="col-md-6 wg-text-center offset-md-2">
                    <div class="wg-store-item-wrapper">
                        <div class="">
                            <div class="">
                                <img src="{{asset('storage/suscribe_products/'.$offer->image)}}" class="wg-store-img">
                            </div>
                            <div class="">
                                <h5 class="wg-product-name">{{$offer->name}}</h5>
                                <p class="wg-product-price">{{$offer->egg_shell_points}}pts
                                
                                </p>
                                <div class="">
                                    <p>Making this request will subtract {{$offer->egg_shell_points}}pts from your balance</p>
                                    
                                    <a class="wg-colored-link " href="#" onclick="event.preventDefault();history.back()" style="font-size:1.3em;background-color:#d3d0d0;">
                                        <i class="fa fa-arrow"> Back</i>
                                    </a>
                                    <a class="wg-colored-link wg-float-right" href="#" onclick="event.preventDefault();document.getElementById('offer-order-form').submit();" style="font-size:1.3em;">
                                        <i class="fa fa-arrow"> Make Request</i>
                                    </a>

                                    <form id="offer-order-form" action="{{route('store.offer.order')}}" method="POST">

                                        {{csrf_field()}}
                                        <input type="hidden" name="offer_id" value="{{$offer->id}}">

                                    </form>
                                </div>
                                
                            </div>

                            
                        </div>
                    </div>
                    <div class="wg-full-width wg-height-30px"></div>
                </div>
            </div>
        </div>
    </div>
   
@endsection
