@extends('layouts.app')
@section('title')
    Ifarm-Home
@endsection
@section('content')
   
<div class="swiper-container hero-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide hero-content-wrap" style="max-height:600px;">
                <img src="images/bag1.jpg" alt="">

                <div class="hero-content-overlay position-absolute w-100 h-100">
                    <div class="container h-100">
                        <div class="row h-100">
                            <div class="col-12 col-lg-8 d-flex flex-column justify-content-center align-items-start">
                                <header class="entry-header">
                                    <h1>IFARM  GROUP</h1>
                                    <h4>Recyclers - Food Processing & Packaging - Exports</h4>
                                </header><!-- .entry-header -->

                                <div class="entry-content mt-4">
                                    <p>
                                        We aren't short of Resources, we are only short of recycling minds.
                                    </p>
                                </div><!-- .entry-content -->

                                <footer class="entry-footer d-flex flex-wrap align-items-center mt-5">
                                    
                                    <a href="{{route('about')}}" class="btn orange-border">Read More</a>
                                </footer><!-- .entry-footer -->
                            </div><!-- .col -->
                        </div><!-- .row -->
                    </div><!-- .container -->
                </div><!-- .hero-content-overlay -->
            </div><!-- .hero-content-wrap -->

            <div class="swiper-slide hero-content-wrap" style="max-height:600px;">
                <img src="images/bag2.jpg" alt="">

                <div class="hero-content-overlay position-absolute w-100 h-100">
                    <div class="container h-100">
                        <div class="row h-100">
                            <div class="col-12 col-lg-8 d-flex flex-column justify-content-center align-items-start">
                                 <header class="entry-header">
                                    <h1>IFARM RECYCLERS</h1>
                                    <h4>Recycle - Don't Toss</h4>
                                </header><!-- .entry-header -->

                                <div class="entry-content mt-4">
                                    <p>
                                        We aren't short of Resources, we are only short of recycling minds.
                                    </p>
                                </div><!-- .entry-content -->

                                <footer class="entry-footer d-flex flex-wrap align-items-center mt-5">
                                   
                                    <a href="{{route('about')}}" class="btn orange-border">Read More</a>
                                </footer><!-- .entry-footer -->
                            </div><!-- .col -->
                        </div><!-- .row -->
                    </div><!-- .container -->
                </div><!-- .hero-content-overlay -->
            </div><!-- .hero-content-wrap -->

            <div class="swiper-slide hero-content-wrap" style="max-height:600px;">
                <img src="images/bag3.jpg" alt="">

                <div class="hero-content-overlay position-absolute w-100 h-100">
                    <div class="container h-100">
                        <div class="row h-100">
                            <div class="col-12 col-lg-8 d-flex flex-column justify-content-center align-items-start">
                                <header class="entry-header">
                                    <h1>IFARM YOU EAT</h1>
                                    <h4>Locally sourced and carefully packaged</h4>
                                </header><!-- .entry-header -->

                                <div class="entry-content mt-4">
                                    <!-- <p>
                                        We aren't short of Resources, we are only short of recycling minds.
                                    </p> -->
                                </div><!-- .entry-content -->

                                <footer class="entry-footer d-flex flex-wrap align-items-center mt-5">
                                   
                                    <a href="{{route('about')}}" class="btn orange-border">Read More</a>
                                </footer><!-- .entry-footer -->
                            </div><!-- .col -->
                        </div><!-- .row -->
                    </div><!-- .container -->
                </div><!-- .hero-content-overlay -->
            </div><!-- .hero-content-wrap -->
        </div><!-- .swiper-wrapper -->

        <div class="pagination-wrap position-absolute w-100">
            <div class="container">
                <div class="swiper-pagination"></div>
            </div><!-- .container -->
        </div><!-- .pagination-wrap -->

        <!-- Add Arrows -->
        <div class="swiper-button-next flex justify-content-center align-items-center">
            <span><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1171 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"/></svg></span>
        </div>

        <div class="swiper-button-prev flex justify-content-center align-items-center">
            <span><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1203 544q0 13-10 23l-393 393 393 393q10 10 10 23t-10 23l-50 50q-10 10-23 10t-23-10l-466-466q-10-10-10-23t10-23l466-466q10-10 23-10t23 10l50 50q10 10 10 23z"/></svg></span>
        </div>
    </div><!-- .hero-slider -->

    <div class="home-page-icon-boxes">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                    <div class="icon-box active" style="background: #A8CF45;">
                        <figure class="d-flex justify-content-center">
                            <img src="{{asset('images/hands-gray.png')}}" alt="">
                            <img src="{{asset('images/hands-white.png')}}" alt="">
                        </figure>

                        <header class="entry-header">
                            <h3 class="entry-title"><a href="{{route('volunteer.register.form')}}">Become A Volunteer</a></h3>
                        </header>

                        <div class="entry-content">
                           <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris quis aliquam. </p> -->
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                    <div class="icon-box">
                        <figure class="d-flex justify-content-center">
                            <img src="{{asset('images/donation-gray.png')}}" alt="">
                            <img src="{{asset('images/donation-white.png')}}" alt="">
                        </figure>

                        <header class="entry-header">
                            <h3 class="entry-title"><a href="{{route('suscriber.register.form')}}">Become A Suscriber</a></h3>
                        </header>

                        <div class="entry-content">
                           <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris quis aliquam. </p> -->
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                    <div class="icon-box">
                        <figure class="d-flex justify-content-center">
                            <img src="{{asset('images/charity-gray.png')}}" alt="">
                            <img src="{{asset('images/charity-white.png')}}" alt="">
                        </figure>

                        <header class="entry-header">
                            <h3 class="entry-title"><a href="{{route('blog.index')}}"> About AgricBiz </a></h3>
                        </header>

                        <div class="entry-content">
                           <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris quis aliquam. </p> -->
                        </div>
                    </div>
                </div>
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .home-page-icon-boxes -->

    <div class="home-page-welcome">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 order-2 order-lg-1">
                    <div class="welcome-content">
                        <header class="entry-header">
                            <h2 class="entry-title">We are an Agricultural Waste Recycling and Social Benefit Venture</h2>
                        </header><!-- .entry-header -->

                        <div class="entry-content mt-5">
                            <p>
                            Ifarm is a leading Agricultural waste recycling company in Nigeria that creates value from daily agricultural waste generated on the farm to help  kick off chemicals which are detrimental to human health by recycling these agricultural waste into bio fertilizers to produce more healthy crops and livestocks and promote organic farming.
                        </p>
                        </div><!-- .entry-content -->

                        <div class="entry-footer mt-5">
                            <a href="{{route('about')}}" class="btn gradient-bg mr-2" style="color:white;">Read More</a>
                        </div><!-- .entry-footer -->
                    </div><!-- .welcome-content -->
                </div><!-- .col -->

                <div class="col-12 col-lg-6 mt-4 order-1 order-lg-2">
                   <!-- <img src="{{asset('images/welcome.png')}}" alt="welcome" > -->
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .home-page-icon-boxes -->

    
    
                            

    <!-- <div class="home-page-limestone">
        <div class="container">
            <div class="row align-items-end">
                <div class="coL-12 col-lg-6">
                    <div class="section-heading">
                        <h2 class="entry-title">We love to help all the children that have problems in the world. After 15 years we have many goals achieved.</h2>

                        <p class="mt-5">Dolor sit amet, consectetur adipiscing elit. Mauris tempus vestib ulum mauris quis aliquam. Lorem ipsum dolor sit amet.</p>
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="milestones d-flex flex-wrap justify-content-between">
                        <div class="col-12 col-sm-4 mt-5 mt-lg-0">
                            <div class="counter-box">
                                <div class="d-flex justify-content-center align-items-center">
                                    <img src="images/teamwork.png" alt="">
                                </div>

                                <div class="d-flex justify-content-center align-items-baseline">
                                    <div class="start-counter" data-to="120" data-speed="2000"></div>
                                    <div class="counter-k">K</div>
                                </div>

                                <h3 class="entry-title">Children helped</h3>
                            </div>
                        </div>

                        <div class="col-12 col-sm-4 mt-5 mt-lg-0">
                            <div class="counter-box">
                                <div class="d-flex justify-content-center align-items-center">
                                    <img src="images/donation.png" alt="">
                                </div>

                                <div class="d-flex justify-content-center align-items-baseline">
                                    <div class="start-counter" data-to="79" data-speed="2000"></div>
                                </div>

                                <h3 class="entry-title">Water wells</h3>
                            </div>
                        </div>

                        <div class="col-12 col-sm-4 mt-5 mt-lg-0">
                            <div class="counter-box">
                                <div class="d-flex justify-content-center align-items-center">
                                    <img src="{{asset('images/dove.png')}}" alt="">
                                </div>

                                <div class="d-flex justify-content-center align-items-baseline">
                                    <div class="start-counter" data-to="253" data-speed="2000"></div>
                                </div>

                                <h3 class="entry-title">Volunteeres</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>.our-causes -->
@endsection