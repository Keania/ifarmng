@extends('layouts.app')
@section('title')
    Shopping Cart
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection
@section('content')
<div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Shopping Cart</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="news-wrap">
        <div class="container">
            <div class="row">

                <!-- <div class="wg-full-width wg-height-100px"></div> -->

                <div class="col-md-10 offset-md-1">

                    <div >
                        <div >
                            @if(count($cartItems) > 0)
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Item Name</th>
                                            <th>Item Price</th>
                                            <th>Item Description</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($cartItems as $cartItem)
                                        
                                            <tr>
                                                <td>{{$cartItem->name}}</td>
                                                <td>{{$cartItem->price}}</td>
                                                <td>{{substr($cartItem->options->desc,0,50)}}.....</td>
                                                <td>
                                                    <a href="{{route('cart.remove')}}" onclick="event.preventDefault();document.getElementById('remove{{$cartItem->id}}').submit();"><i class="fa fa-trash fa-lg"></i></a>
                                                    <form id="remove{{$cartItem->id}}" method="POST" action="{{route('cart.remove')}}">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="id" value="{{$cartItem->rowId}}">
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3">Total</th>
                                            <th colspan="3">{{Cart::total()}}</th>
                                        </tr>
                                    </tfoot>
                                </table>

                                

                                <div class="wg-full-width wg-height-30px"></div>

                                <div>
                                   
                                    <a href="{{route('order.create')}}" class="btn gradient-bg mr-2 wg-float-right">Checkout</a>
                                    
                                    <form method="POST" action="{{route('cart.destroy')}}" class="wg-float-right">
                                        {{csrf_field()}}
                                        <input type="submit" name="submit" class="btn" value="Clear Cart">
                                    </form>
                                </div>
                            @else
                                <div>
                                    <h3>Shopping Cart is empty</h3>
                                </div>
                            @endif
                            
                        </div>
                    </div>

                </div>
               
            </div>
        </div>
    </div>
@endsection
