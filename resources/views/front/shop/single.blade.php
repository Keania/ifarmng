@extends('layouts.app')
@section('title')
Ifarm - Shop
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    <style>

.wg-sm-product-btns {

padding:0.6rem 0.3rem;
width:100%;
}

    </style>
@endsection
@section('content')

<div class="news-wrapper">

    <div class="container">

        <div class="wg-full-width wg-height-30px"></div>
        

        <div class="row">


            <div class="col-md-6 offset-md-2 wg-text-center">
               
                
                <div class="wg-store-item-wrapper">
                    <div class="wg-store-item">
                        <div class="wg-product-image">
                            <img src="{{asset('storage/products/'.$product->photo)}}" class="wg-store-img">
                        </div>
                        <div class="wg-product-description">
                            <h5 class="wg-product-name">{{$product->name}}</h5>
                            <p class="wg-product-price"> ₦{{number_format($product->price)}}
                            </p>
                            <div class="">
                                <a href="#" class="btn btn-sm wg-sm-product-btns" onclick="event.preventDefault();addToCart({{$product->id}});">
                                    <i class="fa fa-shopping-cart"></i> Cart
                                </a>
                                
                               
                            </div>
                            
                           
                            
                        </div>

                        
                    </div>
                </div>
                <div class="wg-full-width wg-height-30px"></div>
            </div>
            

        </div>

        <div class="row">
            <div class="col-md-6 offset-md-2 wg-luicid-font">
                <h5>Product Description</h5>
                <span class="underline"></span>
                <p>
                    {{$product->desc}}
                </p>
            </div>
        </div>

        <div class="wg-full-width wg-height-30px"></div>
    </div>
</div>
@endsection