@extends('layouts.app')
@section('title')
Ifarm - Shop
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

    <style>

        .page-item.active .page-link {
            background-color : #8bc34a;
            border-color: #8bc34a;
        }

       
        .wg-sm-product-btns {

            padding:0.6rem 0.3rem;
            width:100%;
        }

        .news-page .page-header {

            background: url(images/ifarmshop.jpeg);
            background-position:bottom left;
        }

        @media(max-width:765px){

            
        }

        
    </style>
@endsection
@section('content')
<div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Shop</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="news-wrap">

        <div class="container">

            <div class="row">
               
                   @foreach($products as $product)
                   
                        <div class="col-6 wg-text-center">

                            <div class="wg-store-item-wrapper">

                                <div class="wg-store-item">

                                    <div class="wg-product-image">
                                        <img src="{{asset('storage/products/'.$product->photo)}}" class="wg-store-img">
                                    </div>

                                    <div class="wg-product-description">

                                        <h5 class="wg-product-name">{{$product->name}}</h5>

                                        <p class="wg-product-price"> ₦{{number_format($product->price)}}

                                        </p>
                                        <div class="row" >

                                            <div class="col-12 mb-3">

                                                <a href="#" style="color:white;font-size:0.8rem;" class="btn btn-sm wg-sm-product-btns" onclick="event.preventDefault();addToCart({{$product->id}});" >
                                                    <i class="fa fa-shopping-cart"></i> Cart Item
                                                </a>

                                            </div>

                                            <div class="col-12 mb-3">

                                                <a style="color:white;"class="btn btn-sm wg-sm-product-btns" href="{{route('shop.single',['product_id'=>$product->id])}}">
                                                    <i class="fa fa-desktop"> More</i>
                                                </a>

                                            </div>
                                            
                                            
                                           
                                        </div>
                                        <!-- <div class="wg-product-text">
                                            <br>
                                            <p>{{substr($product->desc,0,50)}}.....</p>
                                        </div> -->
                                        
                                    </div>

                                    
                                </div>
                            </div>
                            <div class="wg-full-width wg-height-30px"></div>
                        </div>
                        
                    @endforeach

                   
                   
           


            </div>

            <div class="row text-center">

                <div style="margin:auto">{{$products->links()}}</div>

            </div>

            
        </div>
    </div>
   
@endsection
