@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Order Success</h1>
            </div>
        </div>
    </div>
</div>

<div class="welcome-wrap">
        <div class="container">
            <div class="row" style="padding-top:3.5rem;padding-bottom:2rem;">
                <div class="col-12 col-lg-6 order-2 order-lg-1 offset-lg-3">
                    <div class="welcome-content">
                      
                        <div class="">
                            <p style="color:#A8CF45;text-align:center;">
                                <i class="fa fa-shopping-cart" style="font-size:5rem;"></i>
                            </p>
                           <p style="color:#6b6565;font-size:1.3rem;text-align:center;">
                              Your order was successfull<br>
                              
                              OrderID : #{{$order->ref}}
                           </p>
                        </div><!-- .entry-content -->

                        <div class="entry-footer mt-5" style="text-align:center">
                            <a href="{{route('dashboard')}}" class="btn gradient-bg mr-2"id="read__more__toggler" style="color:white;">Dashboard</a>
                        </div><!-- .entry-footer -->
                    </div><!-- .welcome-content -->
                </div><!-- .col -->

               
            </div><!-- .row -->
            
        </div><!-- .container -->
    </div><!-- .home-page-icon-boxes -->

@endsection
