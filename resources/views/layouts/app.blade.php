<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    <!-- ElegantFonts CSS -->
    <link rel="stylesheet" href="{{asset('css/elegant-fonts.css')}}">

    <!-- themify-icons CSS -->
    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{asset('css/swiper.min.css')}}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <style>
        .badge {
            background-color: #A8CF45;
            color:white;
        }
        .fa-shopping-cart {
            font-size:1rem;
        }
        .current-menu-item a{
            border-bottom: 2px solid #A8CF45;
        }

        .donate-btn a {
            padding:15px 30px;
        }
        ul.tabs-nav .tab-nav.active {
            border: 0;
        
            background: -ms-linear-gradient(180deg, rgba(255,90,0,1) 0%, rgba(255,54,0,1) 100%);
            background: linear-gradient(270deg, #8BC34A 0%, #8BC34A 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff5a00', endColorstr='#ff3600',GradientType=1 );
            color: #fff;
        }
    
    </style>
    @yield('styles')
</head>
<body class="single-page news-page">
    <header class="site-header" >
        <div class="top-header-bar">
            <div class="container">
                <div class="row flex-wrap justify-content-center justify-content-lg-between align-items-lg-center">
                    <div class="col-12 col-lg-8 d-none d-md-flex flex-wrap justify-content-center justify-content-lg-start mb-3 mb-lg-0">
                       <!--  <div class="header-bar-email">
                            MAIL: <a href="#">contact@ourcharity.com</a>
                        </div><!-- .header-bar-email -->

                        <!-- <div class="donate-btn">
                            <a href="#">Donate Now</a>
                          
                        </div> -->
                    </div><!-- .col -->

                    <div class="col-12 col-lg-4 d-flex flex-wrap justify-content-center justify-content-lg-end align-items-center">
                        <div class="donate-btn">
                            @if(Auth::user())
                                <a href="{{route('dashboard')}}"><i class="fa fa-user"></i>  {{Auth::user()->name}}</a>
                                <a href="{{route('logout')}}" style="display:inline-block;" onclick="event.preventDefault();document.getElementById('logout').submit();"><i class="fa fa-lock"></i> Sign Out</a>
                                <form method="POST" action="{{route('logout')}}" id="logout">
                                    {{csrf_field()}}
                                </form>
                            @else
                            <a href="{{route('volunteer.register.form')}}">Volunteer</a>
                            <a href="{{route('register')}}">Sign Up</a>
                            @endif
                            
                        </div><!-- .donate-btn -->
                       
                       <!-- .donate-btn -->
                        
                    </div><!-- .col -->
                </div><!-- .row -->
            </div><!-- .container -->
        </div><!-- .top-header-bar -->

        <div class="nav-bar">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                        <div class="site-branding d-flex align-items-center">
                           <a class="d-block" href="{{route('home')}}" rel="home"><img class="d-block" src="{{asset('images/logo.png')}}" alt="logo"></a>
                        </div><!-- .site-branding -->

                        <nav class="site-navigation d-flex justify-content-end align-items-center">
                            <ul class="d-flex flex-column flex-lg-row justify-content-lg-end align-content-center">
                                <li class="{{request()->is('/') ?'current-menu-item':''}}"><a href="{{route('home')}}">Home</a></li>
                                <li class="{{request()->is('about') ?'current-menu-item':''}}"><a href="{{route('about')}}">About us</a></li>
                                <li class="{{request()->is('shop') ?'current-menu-item':''}}"><a href="{{route('shop.index')}}">Shop</a></li>
                                <li class="{{request()->is('blog') ?'current-menu-item':''}}"><a href="{{route('blog.index')}}">Blog</a></li>
                                <li class="{{request()->is('gallery') ?'current-menu-item':''}}"><a href="{{route('gallery')}}">Gallery</a></li>
                                @if(!Auth::check())
                                <li  class="{{request()->is('login') ?'current-menu-item':''}}"><a href="{{route('login')}}">Login</a></li>
                                @endif
                                
                                <li  class="{{request()->is('contact') ?'current-menu-item':''}}"><a href="{{route('contact')}}">Contact</a></li>
                                <li  class="{{request()->is('cart') ?'current-menu-item':''}}"><a href="{{route('cart.index')}}"><i class="fa fa-shopping-cart"><span class="badge badge-secondary" id="cart_num">{{count(Cart::content())}}</span></i></a></li>
                            </ul>
                        </nav><!-- .site-navigation -->

                        <div class="hamburger-menu d-lg-none">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div><!-- .hamburger-menu -->
                    </div><!-- .col -->
                </div><!-- .row -->
            </div><!-- .container -->
        </div><!-- .nav-bar -->
    </header><!-- .site-header -->
    @if(session()->has('error'))
        
        <div class="alert alert-warning">
            <i class="fa fa-warning"></i>{{session()->get('error')}}
        </div>
    @elseif(session()->has('success'))
        <div class="alert alert-success">
            <i class="fa fa-info"></i> {{session()->get('success')}}
        </div>
    @endif
   
    @if(Auth::check() && !Auth::user()->email_verified())
    
     <div class="alert alert-warning">
            <i class="fa fa-warning"></i>Your Account has not been verified <a href="{{route('resend.verification')}}" style="text-decoration:underline;">Resend Verification Email</a>
        </div>
    @endif

    
    @yield('content')
    
   

    <div class="modal fade in" tabindex="-1" role="dialog" id="cartModal" data-backdrop="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> <i class="fa fa-info"></i>     Cart</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="modal-product-name"></p>
                    
                </div>
                <div class="modal-footer">

                    <div class="pull-left">
                        <a href="{{route('cart.index')}}" class="btn"><i class="fa fa-shopping-cart fa-lg"></i>  VIEW CART</a>
                    </div>
                    <div class="pull-right">
                        <a  href="{{route('shop.index')}}" class="btn"> <i class="fa fa-chevron-right fa-lg"></i> CONTINUE SHOPPING
                        </a>
                
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="site-footer">
        <div class="footer-widgets">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="foot-about">
                            <h2><a class="foot-logo" href="#"><img src="{{asset('images/foot-logo.png')}}" alt=""></a></h2>

                            <p>
                                <b>IFARM</b> is a leading Agricultural waste recycling and social benefit venture in Nigeria that creates value from generated poultry waste.
                                <br>
                                We have created sustainable alternative for handling  poultry waste through recycling them into an eco-friendly product of diverse application.
                            </p>

                            <ul class="d-flex flex-wrap align-items-center">
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div><!-- .foot-about -->
                    </div><!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                        <h2>Useful Links</h2>

                        <ul>
                            <li><a href="{{route('suscriber.register.form')}}">Become a Suscriber</a></li>
                            <li><a href="{{route('volunteer.register.form')}}">Become  a Volunteer</a></li>
                            <li><a href="#">Testimonials</a></li>
                            <li><a href="{{route('shop.index')}}">Shop</a></li>
                            <li><a href="{{route('gallery')}}">Gallery</a></li>
                            <li><a href="{{route('blog.index')}}">Blog</a></li>
                        </ul>
                    </div><!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                        <div class="foot-latest-news">
                            <h2>Latest News</h2>
                            @php
                                $latest_posts = App\Blog\Post::latest()->take(3)->get();
                            @endphp

                            @if($latest_posts->isNotEmpty())
                               
                                <ul>
                                     @foreach($latest_posts as $latest_post)
                                        <li>
                                            <h3><a href="{{route('post.single',['id'=>$latest_post->id])}}">{{$latest_post->post_title}}</a></h3>
                                            <div class="posted-date">{{date("M d , Y",strtotime($latest_post->created_at))}}</div>
                                        </li>
                                    @endforeach
                                </ul>

                            @endif
                        </div><!-- .foot-latest-news -->
                    </div><!-- .col -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                        <div class="foot-contact">
                            <h2>Contact</h2>

                            <ul>
                                <li><i class="fa fa-phone"></i><span>+234 (0)70 3161 6551</span></li>
                                <li><i class="fa fa-envelope"></i><span>ifarmng1@gmail.com</span></li>
                                <li><i class="fa fa-map-marker"></i><span> <p>Port Harcourt <br>Rivers State</p></span></li>
                            </ul>
                        </div><!-- .foot-contact -->

                        <div class="subscribe-form">
                            <form class="d-flex flex-wrap align-items-center">
                                <input type="email" placeholder="Your email">
                                <input type="submit" value="send">
                            </form><!-- .flex -->
                        </div><!-- .search-widget -->
                    </div><!-- .col -->
                </div><!-- .row -->
            </div><!-- .container -->
        </div><!-- .footer-widgets -->

        <div class="footer-bar">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p class="m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div><!-- .col-12 -->
                </div><!-- .row -->
            </div><!-- .container -->
        </div><!-- .footer-bar -->
    </footer><!-- .site-footer -->

    <script type='text/javascript' src='{{asset("js/jquery.js")}}'></script>
    <script type='text/javascript' src='{{asset("js/jquery.collapsible.min.js")}}'></script>
    <script type='text/javascript' src='{{asset("js/swiper.min.js")}}'></script>
    <script type='text/javascript' src='{{asset("js/jquery.countdown.min.js")}}'></script>
    <script type='text/javascript' src='{{asset("js/circle-progress.min.js")}}'></script>
    <script type='text/javascript' src='{{asset("js/jquery.countTo.min.js")}}'></script>
    <script type='text/javascript' src='{{asset("js/jquery.barfiller.js")}}'></script>
    <script type='text/javascript' src='{{asset("js/custom.js")}}'></script>
    <script type='text/javascript' src='{{asset("js/bootstrap.min.js")}}'></script>
<script>
$(document).ready(function(){
    console.log('I found jquery on this page');
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function addToCart(id){
    $.ajax({
        url : "/cart/add",
        data: {id:id},
        type : 'POST',
    }).success(function(data){
        document.getElementById('cart_num').innerHTML = data[0];
        document.getElementById('modal-product-name').innerHTML = data[1].name + " added to cart";
        $('#cartModal').modal('show');
    });
}

</script>

    @yield('script')

</body>
</html>
