<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="{{asset('images/logo.png')}}">


    @yield('styles')
</head>

<body>
    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="{{asset('images/logo.png')}}" alt="Logo" width="100"></a>
                <a class="navbar-brand hidden" href="./"><img src="{{asset('images/logo.png')}}" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{route('admin.dashboard')}}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <h3 class="menu-title">Site Features</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-signal"></i>My Products</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="{{route('admin.add.product')}}">Add Products</a></li>
                            
                            <li><i class="fa fa-id-badge"></i><a href="{{route('admin.view.products')}}">View Products</a></li>
                            
                        
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-signal"></i>Suscriber Offers</a>
                        <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-puzzle-piece"></i><a href="{{route('get.suscriber.offer.form')}}">Add Offers</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="{{route('admin.suscriber_products')}}">View Offers</a></li>
                            
                        
                        </ul>
                    </li>


                    <li class="menu-item-has-children active dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-rss"></i>Blog</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-rss"></i><a href="{{route('admin.add.post')}}">Add Post</a></li>
                            <li><i class="fa fa-rss"></i><a href="{{route('admin.view.posts')}}">View Post</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children active dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-shopping-cart"></i>Orders</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="{{route('admin.cash.request')}}">Cash Request Orders</a></li>
                            <li><i class="fa fa-table"></i><a href="{{route('admin.suscription.orders')}}">Suscribers Orders</a></li>
                            <li><i class="fa fa-table"></i><a href="{{route('admin.product.orders')}}">Ifarm Product Orders</a></li>
                           
                        </ul>
                    </li>
                    <li class="menu-item-has-children active dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user-md"></i>Users</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="{{route('all.users')}}">All Customers</a></li>
                            <li><i class="fa fa-table"></i><a href="{{route('all.suscribers')}}">Suscribers</a></li>
                            <li><i class="fa fa-table"></i><a href="{{route('all.volunteers')}}">Volunteers</a></li>
                        </ul>
                    </li>

                    <li><a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout').submit();"><i class="menu-icon fa fa-sign-out"></i>Sign Out</a></li>
                    <form method="POST" action="{{route('logout')}}" id="logout">
                        {{csrf_field()}}
                    </form>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
    @yield('contents')

    @yield('scripts')
</body>