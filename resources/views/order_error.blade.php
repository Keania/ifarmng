@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Error Transaction</h1>
            </div>
        </div>
    </div>
</div>

<div class="welcome-wrap">
        <div class="container">
            <div class="row" style="padding-top:3.5rem;padding-bottom:2rem;">
                <div class="col-12 col-lg-6 order-2 order-lg-1 offset-lg-3">
                    <div class="welcome-content">
                      
                        <div class="entry-content mt-5">
                            <p style="color:#ff0000b5;font-size:4rem;text-align:center;">
                                <i class="fa fa-warning"></i>
                            </p>
                           <p style="color:#6b6565;font-size:1.3rem;text-align:center;">
                               We encountered an error while processing your transaction if your claim is legit contact admin.
                           </p>
                        </div><!-- .entry-content -->

                        <div class="entry-footer mt-5" style="text-align:center">
                            <a href="{{route('contact')}}" class="btn gradient-bg mr-2"id="read__more__toggler">Contact Us</a>
                        </div><!-- .entry-footer -->
                    </div><!-- .welcome-content -->
                </div><!-- .col -->

               
            </div><!-- .row -->
            
        </div><!-- .container -->
    </div><!-- .home-page-icon-boxes -->

@endsection
