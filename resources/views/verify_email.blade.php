<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ifarm Contact Email</title>
    <style>
            header{
                width:96%;
                margin:auto;
                height:80px;
              
                padding:2%;
              
            }

            section#container {
                width:75%;
                margin:auto;
                font-family:cursive;
                font-size: 1rem;
            }
            span#slogan {
                float:right;
                color:white;
            }
            p#salutation {
                font-weight:bold;
            }
            p#verifyButton {
                text-align:center;
            }
            #verifyButton button {
                background-color:#a8cf45;
                color:white;
                font-size:1.1rem;
                padding:10px;
                border:none;
                outline:none;
                border-radius:5px;
            }
            #verifyButton a {
                color:white;
                text-decoration:none;
            }
            p {
                margin-bottom:2rem;
            }
    
    </style>
</head>
<body>

    <section id="container">

        <header>
            <span id="slogan" style="color:black;">Recycle don't toss ....</span>
            <img src="{{asset('images/logo.png')}}">
        </header>
        <section>
            <p id="salutation">Dear {{$name}},</p>
            <p>Thanks for Signing up with---Ifarm---, Please use the verify button below to verify your Email address with us, 
                or copy the link and paste it on your browser URL if the button is not working for you.</p>

                <p id="verifyButton">
                    <button><a href="https://ifarmng.com/verify/email/{{$token}}">Verifiy Email</a></button>
                </p>

            <p><b>PLEASE NOTE:</b> To complete account activation, you will need to verify that your information is correct as filled on our website, 
                kindly use your account verification section of your profile to do this.
            </p>
            <p style="margin-bottom:2rem; text-align:center">

                <a href="https://ifarmng.com/verify/email/{{$token}}">https://ifarmng.com/verify/email/{{$token}}</a>
            </p>




        </section>
        <footer>
            Thank you.<br>
            Ifarm Team.<br>
            Recycling Nigeria for good.<br>
            ----------------------------------------------------<br>
            #20 Ada-George Road, Port Harcourt, Rivers State, Nigeria.<br>
            ----------------------------------------------------<br>
            Contact us on info@ifarmng.com<br>
            ----------------------------------------------------<br>

        </footer>
    </section>
    
</body>
</html>