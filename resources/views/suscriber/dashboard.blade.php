
@extends('layouts.app')
@section('title')
Ifarmng - Dashboard
@endsection
@section('content')
@php
    $user = Auth::user();
  
   
@endphp
    <style>
            .order-tables {
                margin-top:2rem;
                margin-bottom: 2rem;
            }
    
    </style>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Dashboard</h1>
                </div><!-- .col -->
            </div><!-- .row -->
            
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="news-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-12">
                    <div class="tabs">
                        <ul class="tabs-nav d-flex">
                            
                            <li class="tab-nav d-flex justify-content-center align-items-center" data-target="#tab_2">My Profile</li>
                            <li class="tab-nav d-flex justify-content-center align-items-center" data-target="#tab_3">My Orders</li>
                            <li class="tab-nav d-flex justify-content-center align-items-center" data-target="#tab_4">Egg Shell Record</li>
                        </ul>

                        <div class="tabs-container">
                            

                            <div id="tab_2" class="tab-content">
                                <div class="">
                                    <h2>Profile</h2>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Registration Number:</span>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <span>{{$user->reg_number}}</span>
                                        
                                        </div>
                                    
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Name:</span>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <span>{{$user->name}}</span>
                                        
                                        </div>
                                    
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Email:</span>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <span>{{$user->email}}</span>
                                        
                                        </div>
                                    
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Phone:</span>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <span>{{$user->phone}}</span>
                                        
                                        </div>
                                    
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Address:</span>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <span>{{$user->address}}</span>
                                        
                                        </div>
                                    
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Suscription Type:</span>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <span>{{$user->suscription_type}}</span>
                                        
                                        </div>
                                    
                                    </div>
                                    <br>
                                    <br>

                                    <div>
                                        <button  class="btn gradient-bg mr-2" data-toggle="modal" data-target="#editProfile">Edit Profile</button>
                                    
                                    </div>

                                    <div class="modal fade" id="editProfile" tabindex="-1" role="dialog" aria-labelledby="addPoint" aria-hidden="true" data-backdrop="false">

                                        <div class="modal-dialog " role="document">

                                            <div class="modal-content card">

                                                <div class="modal-header card-header">
                                                    <h5 class="modal-title" id="editModalLabel">Edit Profile</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>

                                                <div class="modal-body card-body">

                                                    <form id="editProfileForm" action="{{route('update.profile.info')}}" method="POST" class="form-horizontal">
                                                        {{csrf_field()}}
                                                        <div class="form-group">
                                                            <label class="form-control-label">Name</label>
                                                            <input type="text"class="form-control" name="name" value="{{$user->name}}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="form-control-label">Phone Number</label>
                                                            <input type="text"class="form-control" name="phone_number" value="{{$user->phone_number}}">
                                                        
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="form-control-label">Address</label>
                                                            <input type="text" class="form-control" name="address" value="{{$user->address}}">
                                                        
                                                        </div>

                                                       
                                                    
                                                    </form>
                                                
                                                </div>

                                                <div class="modal-footer card-footer">

                                                    <button type="button" class="btn " data-dismiss="modal">Cancel</button>
                                                    <button type="button" onclick="document.getElementById('editProfileForm').submit();"class="btn wg-colored-link">Update Info</button>
                                                </div>
                                            
                                            </div>
                                        
                                        </div>
                                    
                                    </div>
                                        

                                    

                                   

                                </div><!-- .col -->

                            </div>

                            <div id="tab_3" class="tab-content">
                                @php
                                    $counter = 0;
                                @endphp
                                @if($user->cashRequestOrders->isNotEmpty())

                                    <div class="order-tables">
                                        
                                        <h5>Cash Request Orders</h5>
                                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Amount</th>
                                                    <th>Points Traded</th>
                                                    <th>Account Name</th>
                                                    <th>Account Number</th>
                                                    <th>Bank</th>
                                                    <th>Order status</th>
                                                    <th>Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            
                                            @foreach($user->cashRequestOrders as $cashOrder)
                                                    <tr>
                                                        <td>{{++$counter}}</td>
                                                        <td>{{$cashOrder->amount}}</td>
                                                        <td>{{$cashOrder->egg_shell_point}}</td>
                                                        <td>{{$cashOrder->account_name}}</td>
                                                        <td>{{$cashOrder->account_number}}</td>
                                                        <td>{{$cashOrder->bank}}</td>
                                                        <td> {{getOrderStatusMessage($cashOrder->status)}}</td>
                                                        <td>{{date('d/ m /Y H:i:s',strtotime($cashOrder->created_at))}}</td>
                                                    </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>

                                    
                                @endif

                                @if($user->suscription_orders->isNotEmpty())

                                    <div class="order-tables">
                                        <h5>Suscription Product Orders</h5>
                                    
                                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Date</th>
                                                    
                                                    <th>Product</th>
                                                    <th>Points </th>
                                                    <th>Order status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach($user->suscription_orders as $order)
                                                @php
                                                    
                                                    $product = App\SuscribeProduct::find($order->offer_ordered);
                                                @endphp
                                                    <tr>
                                                        <td>
                                                            {{++$counter}}
                                                        </td>
                                                        <td>
                                                            {{date('d/ m /Y',strtotime($order->created_at))}}
                                                        </td>
                                                        <td>
                                                            {{$product->name}}
                                                        
                                                        </td>
                                                        
                                                        <td>
                                                            {{$product->egg_shell_points}}pts
                                                        
                                                        </td>
                                                        <td>
                                                            {{getOrderStatusMessage($order->status)}}
                                                        
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif

                                @if($user->orders->isNotEmpty())

                                    <div class="order-tables">
                                        <h5> Product Orders</h5>

                                        @foreach($user->orders as $order)
                                            <h6>OrderID : # {{$order->ref}} <span class="badge badge-secondary">{{getOrderStatusMessage($order->status)}}</span></h6>
                                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                                <caption>Order cart</caption>
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Date</th>
                                                    
                                                    <th>Product</th>
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @if($order->orderItems->isNotEmpty())
                                                
                                                    @foreach($order->orderItems as $orderItem)

                                                        @php
                                                            
                                                            $product = App\Product::find($orderItem->product_id);
                                                        @endphp
                                                        <tr>
                                                            <td>
                                                                {{++$counter}}
                                                            </td>
                                                            <td>
                                                                {{date('d/ m /Y',strtotime($orderItem->created_at))}}
                                                            </td>
                                                            <td>
                                                                {{$product->name}}
                                                            
                                                            </td>
                                                        </tr>
                                                    @endforeach


                                                @endif
                                                
                                                
                                            </tbody>
                                        </table>
                                        @endforeach
                                    </div>

                                @endif

                               
                            
                            </div>

                            <div id="tab_4" class="tab-content">
                                @if(isset($user->ESP_Wallet))
                                <div class="row">

                                    <div class="col-md-6"><h5>Available ESPoint Balance</h5></div>
                                    <div class="col-md-3"><h5>{{$user->ESP_Wallet->balance}}pts</h5></div>

                                    @if($user->ESP_Wallet->balance > 0)
                                        <div class="col-md-3">
                                            <a  class="btn gradient-bg mr-2" href="{{route('suscribers_offers')}}">Redeem Points</a>
                                        </div>
                                    @endif

                                </div>
                                @else
                                    <div class="row">
                                        <h4>Your have no Egg Shell Points </h4>
                                    </div>
                                @endif
                                <br>
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Description</th>
                                            <th>Weight</th>
                                            <th>Points Obtained</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        @foreach($user->ES_Points as $eggPoint)
                                        @php
                                            $counter++;
                                        @endphp
                                            <tr>
                                                <td>
                                                    {{$counter}}
                                                </td>
                                                <td>
                                                    {{date('d/ m /Y',strtotime($eggPoint->created_at))}}
                                                </td>
                                                <td>
                                                    {{date('H:i A ',strtotime($eggPoint->created_at))}}
                                                </td>
                                                <td>
                                                    {{$eggPoint->description}}
                                                
                                                </td>
                                                <td>
                                                    {{$eggPoint->weight}}
                                                </td>
                                                <td>
                                                    {{$eggPoint->points}}pts
                                                
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

