@extends('layouts.app')
@section('title')
Ifarm - Review Suscriber Product
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection
@section('content')

<div class="news-wrapper">

    <div class="container">

        <div class="wg-full-width wg-height-30px"></div>
        

        <div class="row">


            <div class="col-md-6 offset-md-2 wg-text-center">
               
                
               
                <div>
                    <div class="col-md-8 offset-md-2">
                        <img src="{{asset('storage/products/'.$product->photo)}}" class="wg-store-img">
                    </div>
                    <br>
                    
                </div>
                <div>

                    <h5>{{$product->name}}</h5>
                    <h4>{{$product->shell_points}} pts</h4>
                    
                </div>
               
                <div class="wg-full-width wg-height-30px"></div>

                

            </div>
            <div class="row">

                <div class="col-md-6 offset-md-2 wg-luicid-font">
                    <h5>Notice</h5>
                    <span class="underline"></span>

                    <p>
                        Continuing with this transaction will subtract {{$product->shell_points}} pts from your total egg shell account. Do you wish to continue ?
                    </p>
                    <p class="row">
                        <a href="#" class="btn">Back</a>
                        <a href="#" class="wg-colored-link">Proceed</a>
                    </p>
                </div>
            </div>
            

        </div>

        

        <div class="wg-full-width wg-height-30px"></div>
    </div>
</div>
@endsection