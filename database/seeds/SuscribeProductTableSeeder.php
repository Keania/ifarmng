<?php

use Illuminate\Database\Seeder;

class SuscribeProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        foreach(range(1,25) as $index=>$value){
            
            DB::table('suscribe_products')->insert([
                'name'=>$faker->name,
                'image'=>'img1.jpg',
                'egg_shell_points'=>rand(500,20000)
            ]);
        }
    }
}
