<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker\Factory::create();

        foreach(range(1,40) as $index=>$value){

            DB::table('posts')->insert([

                'post_title'=>$faker->sentence,
                'post_content'=>$faker->paragraph(2),
                'image_file'=>'img1.jpg'
            ]);
        }
    }
}
