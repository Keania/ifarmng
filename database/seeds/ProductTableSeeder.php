<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker\Factory::create();

        foreach(range(1,10) as $index=>$value){

            DB::table('products')->insert([

                'name'=>$faker->firstNameMale.' Product',
                'price'=>rand(1000,20000),
                'desc'=>$faker->paragraph(1),
                'photo'=>'product.jpg'
               
            ]);
        }
    }
}
