<?php

use Illuminate\Database\Seeder;

class VolunteerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        $service_types = array(
            'Door to Door Pickup',
            'Drop Off Pickup',
            'New Pick Up Point'
        );

        $service_durations = array(
            '1 Month',
            '3 - 4 Months',
            '5 - 6 Months',
            '6months - 1year',
            '1year & Above'
        );

        $availability = array(
            'Ready to start',
            'Always Available',
            'Call to confirm Availability'
        );

        $sources = array(
            'Soial Media',
            'Colleagues',
            'Websites',
            'Magazine',
            'Group Chat'
        );

        foreach(range(1,20) as $index=>$value){

            DB::table('volunteers')->insert([
                
                'name'=>$faker->firstName,
                'email'=>$faker->email,
                'phone'=>$faker->phoneNumber,
                'current_location'=>$faker->address,
                'service_type'=>$faker->randomElement($service_types),
                'duration_of_service'=>$faker->randomElement($service_durations),
                'availability'=>$faker->randomElement($availability),
                'hear_about_us'=>$faker->randomElement($sources),
            ]);
        }
    }
}
