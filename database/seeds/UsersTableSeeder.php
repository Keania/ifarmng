<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        $roles = [1,2,7];

        foreach(range(1,30) as $index=>$value){

            DB::table('users')->insert([
                'name'=>$faker->name,
                'email'=>$faker->email,
                'phone'=>$faker->phoneNumber,
                'address'=>$faker->address,
                'role'=>$faker->randomElement($roles),
                'password'=>bcrypt('favour'),
                'token'=>str_random(17),
            ]);
        }
    }
}
