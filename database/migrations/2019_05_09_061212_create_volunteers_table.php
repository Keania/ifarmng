<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('password')->nullable();           
            $table->string('email');
            $table->string('phone');
            $table->longText('current_location');
            $table->string('service_type');
            $table->string('duration_of_service');
            $table->string('availability');
            $table->string('hear_about_us');
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers');
    }
}
